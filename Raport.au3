#include-once
#include <ScreenCapture.au3>
#include <GDIPlus.au3>
#include <Date.au3>
#include <Memory.au3>

Global $Raport_G_FileName

Func _Raport_HTML($Content1, $Content2="", $Content3="", $Content4="")
    ;#forcedef $Content1, $Content2, $Content3, $Content4
	Local $Raport_G_FileName = StringTrimRight($Main_G_FileLog, 3) & "html"
	Local $sText = ""
    If NOT FileExists($Raport_G_FileName) Then ; If file Raport not exist then create
		; Define HTML file header and style
		Local $sHead = ""
		$sHead = '<html>' & @CRLF & '<head><meta charset="utf-8"></head>' & @CRLF
		$sHead = $sHead & '<style>img{border:3px solid #FF0000;}</style>' & @CRLF
		$sHead = $sHead & '<style>pre{font-family: monospace;}</style>' & @CRLF
		$sHead = $sHead & '<style>pre{font-size: large;}</style>' & @CRLF
		$sHead = $sHead & '<pre>' & @CRLF
		FileOpen($Raport_G_FileName, 258)
        FileWrite($Raport_G_FileName, $sHead)
    EndIf

	Local $i, $Data
	For $i=1 To @NumParams
		$Data = Eval("Content" & $i)	; read data from $Content1 to 4
		If $Data <> "" Then
			If $i > 1 Then	; if 2 or next line then add 11 space left side (indentation on width "[GG:MM:SS] ")
				$sText = $sText & '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			EndIf

			Select
				Case IsHWnd($Data)=1	; Add to raport Captures a screen shot of a specified window handle
					WinActivate($Data)
					Local $hBitMap = _ScreenCapture_CaptureWnd("", $Data)
					_GDIPlus_Startup()
					Local $hImage = _GDIPlus_BitmapCreateFromHBITMAP ($hBitMap)
					Local $bString = __Image2BinaryString($hImage)
					If $i = 1 Then
						$sText = $sText & @CRLF & '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					EndIf
					$sText = $sText & '<img src="data:image/png;base64,' & __ConvertToBase64($bString) & '"/>'
					_GDIPlus_ImageDispose($hImage)
					_WinAPI_DeleteObject($hBitMap)
					_GDIPlus_Shutdown()

				Case StringInStr($Data,"[SCREEN]:") = 1 And $Data <> "[SCREEN]:FULL"	; Add ta raport	Captures a screen shot of a specified window Title or Class
					Local $hWnd = WinGetHandle(StringTrimLeft($Data, 9))
					If WinExists($hWnd) Then
						WinActivate($hWnd)
						Local $hBitMap = _ScreenCapture_CaptureWnd("", $hWnd)
						_GDIPlus_Startup()
						Local $hImage = _GDIPlus_BitmapCreateFromHBITMAP ($hBitMap)
						Local $bString = __Image2BinaryString($hImage)
						If $i = 1 Then
							$sText = $sText & @CRLF & '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						EndIf
						$sText = $sText & '<img src="data:image/png;base64,' & __ConvertToBase64($bString) & '"/>'
						_GDIPlus_ImageDispose($hImage)
						_WinAPI_DeleteObject($hBitMap)
						_GDIPlus_Shutdown()
					Else
						$sText = $sText & 'ERROR: Window "' & $Data & '" NOT Exists.'
					EndIf

				Case $Data = "[SCREEN]:FULL"					; Add to raport full screen capture
					Local $hBitMap = _ScreenCapture_Capture("")
					_GDIPlus_Startup()
					Local $hImage = _GDIPlus_BitmapCreateFromHBITMAP ($hBitMap)
					Local $bString = __Image2BinaryString($hImage)
					If $i = 1 Then
						$sText = $sText & @CRLF & '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					EndIf
					$sText = $sText & '<img src="data:image/png;base64,' & __ConvertToBase64($bString) & '"/>'
					_GDIPlus_ImageDispose($hImage)
					_WinAPI_DeleteObject($hBitMap)
					_GDIPlus_Shutdown()

				Case Else
					If StringInStr($Data, @CRLF) > 0 Then
						$Data = StringReplace($Data, @CRLF, @CRLF & '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')
					EndIf
					$sText = $sText & $Data

			EndSelect

			$sText = $sText & @CRLF	; add new line
		EndIf
	Next
    FileWrite($Raport_G_FileName, "[" & _NowTime(5) & "]&nbsp;" & $sText &@CRLF)    ; write to file Raport
EndFunc

Func __ConvertToBase64($bString)
    $objXML=ObjCreate("MSXML2.DOMDocument")
    $objNode=$objXML.createElement("b64")
    $objNode.dataType="bin.base64"
    $objNode.nodeTypedValue=Binary($bString)
    Return $objNode.Text
EndFunc

Func __Image2BinaryString($hBitmap) ; based on UEZ code
	Local $sImgCLSID, $tGUID, $tParams, $tData
	$sImgCLSID = _GDIPlus_EncodersGetCLSID("PNG")
	$tGUID = _WinAPI_GUIDFromString($sImgCLSID)
	Local $hStream = _WinAPI_CreateStreamOnHGlobal()
	_GDIPlus_ImageSaveToStream($hBitmap, $hStream, DllStructGetPtr($tGUID))
	Local $hMemory = _WinAPI_GetHGlobalFromStream($hStream)
	Local $iMemSize = _MemGlobalSize($hMemory)
	Local $pMem = _MemGlobalLock($hMemory)
	$tData = DllStructCreate("byte[" & $iMemSize & "]", $pMem)
	Local $bData = DllStructGetData($tData, 1)
	_WinAPI_ReleaseStream($hStream)
	_MemGlobalFree($hMemory)
	Return $bData
EndFunc

