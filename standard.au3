﻿
Global $Katalog_workspace = ""			; ścieżka do katalogu workspace (NIE zawsze będzie C:\POWERFARMER\workspace\)
Global $PlikLog = ""					; zmienna przechowuje nazwe pliku log, który tworzy się w attachments: {ścieżka_do_workspace}\result\attachments\RRRRMMDDGGHHSS_Main.
Global $PlikLogBusiness = ""			; zmienna przechowuje nazwe pliku log, który tworzą się w attachments: {ścieżka_do_workspace}\result\attachments\RRRRMMDDGGHHSS_Main.

Global $logger_result = ""				; wynik wartości "result"
Global $logger_status = ""				; wynik wartości "status": 'failure' lub 'success'
Global $a_logger_returns[0][2]			; tablica do przechowywania wartości zwracanych przez skrypt w sekcji "returns" [nazwa][wartość]
Global $a_logger_errors[3][2] = [["stack_trace",""],["caused_by",""],["screenshot",""]] ; tablica do przechowywania wartości zwracanych przez skrypt w sekcji "errors" [nazwa][wartość]
Global $logger_wynik = ""				; treść results.json do zapisania do pliku
Global $timestart = _NowCalc()			; data: czas uruchomienia skryptu
Global $timeend							; data: czas zakończenia skryptu
Global $application = Null				; hWnd okna Aplikacji
Global $logBizToPF = False				; flaga czy do pola 'Wynik' w PF i do treści e-mail przekazywać również treść loga biznesowego

Global $a_options[0][2]					; tablica przechowująca dane z pliku options.json
Global $a_arguments[0][2]				; tablica przechowująca dane z pliku arguments.json

Global $mailingData = Null				; tablica z danymi do wysyłania mailingu po zakończeniu działania skryptu:
										; [0:uchwyt do klienta mail|1:nazwa funkcji wysyłająca mailing|2: adres email|3:nazwa funkcji generująca tytuł|4:nazwa funkcji generująca treść|5:załączniki rozdzielone ";"|...
										;	6:dodatkowe info w tytule maila|7:nazwa funkcji wyłączająca klienta mail]
										; przykład: [ $oOutlook, "_casepromailing_SendEmail", $OPERATOR_MAIL, "_callcenter_PrepareMailSubject", "_callcenter_PrepareMailContent", $screenShotFileName, $ID_UCZESTNIKA ]

Global $eolHTML = "<br>" & @CRLF		; koniec linii dla loga - dla formatu HTML jest to "<br>"
Global $nofileLogResultEntries[100]		; wpisy do loga bezplikowego
Global $nofileLogResultIndex = 0		; index kolejnego wpisu loga bezplikowego
Global $rodoLogging = True				; czy logować do loggerów plikowych gdy jest uwzględniane rodo

Global $sMsgWorkerError = "Błąd techniczny na stacji uruchomieniowej: "		; prefix komunikatu dla Operatora informujący o błędzie na stacji uruchomieniowej

; wyrażenie regularne wykorzystywane do zamiany znaków ["-" | "+" | "." | ";" | ","] wraz z poprzedzającymi/następującymi spacjami na pojedynczy znak "," - wykorzystywane w funkcji StringRegExpReplace()
Global $REGEX_REPLACE_LIST = "\h+-+\h+|\h+-+|-+\h+|-+|\h+\.+\h+|\h+\.+|\.+\h+|\.+|\h+;+\h+|\h+;+|;+\h+|;+|\h+,+\h+|\h+,+|,+\h+|,+"
Local $oMyError = Null	; zmienna techniczna


#include-once
#include <Array.au3>
#include <Clipboard.au3>
#include <Crypt.au3>
#include <Date.au3>
#include <File.au3>
#include <FileConstants.au3>
#include <GuiComboBox.au3>
#include <MsgBoxConstants.au3>
#include <ScreenCapture.au3>
#include <SQLite.au3>
#include <SQLite.dll.au3>
#include <String.au3>
#include <Timers.au3>
#include <BinaryCall.au3>
#include <Json.au3>
#include <..\..\libraries\autoit_standard_libraries\Main.au3>

; sprawdzenie czy wszystkie używane zmienne są zadeklarowane
;~ Opt("MustDeclareVars", 1)


; #FUNCTION# ====================================================================================================================
; Name ..........: _Start_Skryptu
; Description ...: Funkcja wykonuje sekwencję starową skryptów poprzez ustawienie parametrów, zmiennych, etc.
; Syntax ........: _Start_Skryptu($NazwaSkryptu)
; Parameters ....: $NazwaSkryptu        - nazwa skryptu z którego została funkcja wywołana
; Return values .: None
; Author ........: Piotr Wiśniewski © makeitright.pl
; ===============================================================================================================================
Func _Start_Skryptu($NazwaSkryptu)
	HotKeySet("{PAUSE}", "_Stop")

	$timestart = _NowCalc()
;~ 	$Blad = False ; ustawienie znacznika błędu na Fałsz
;~ 	$BladOpis = ""
	$Katalog_workspace = StringLeft(@ScriptDir,StringInStr(@ScriptDir,"\workspace\") + 10)
	$PlikLog = $Katalog_workspace & "result\attachments\" & "Main_" & StringRegExpReplace($timestart, ":|/|\h+","") & "."	;~ 	StringReplace(StringReplace(StringReplace($timestart,":","")," ",""),"/","") & "."
	$PlikLogBusiness = $Katalog_workspace & "result\attachments\" & "Main_Business_" & StringRegExpReplace($timestart, ":|/|\h+","") & "."
	_Logger("Skrypt: " & @ScriptName, "b")
	_Logger("Start skryptu: " & $timestart, "br")

	_ZaladujArgumentsOptions("options") ; odczyt danych z options.json do tablicy $a_options
	_ZaladujArgumentsOptions("arguments") ; odczyt danych z arguments.json do tablicy $a_arguments

	_SynchronizeWithMain()
EndFunc   ; ==> _Start_Skryptu()


; #FUNCTION# ====================================================================================================================
; Name ..........: _Stop
; Description ...: Funkcja wykorzystywana do obsłuzenia HotKey tzw. "emergency button" zatrzymania skryptu
; Syntax ........: _Stop()
; Parameters ....: None
; Return values .: None
; Author ........: Piotr Wiśniewski © makeitright.pl
; ===============================================================================================================================
Func _Stop()
		_KoniecBlad("Skrypt pmfs_0_symultaniczna_zasilenie_SQLite PRZERWANY przez użytkownika", "pmfs_0_symultaniczna_zasilenie_SQLite", "Przerwano przez użytkownika")
EndFunc		; ==> _Stop()


; #FUNCTION# ====================================================================================================================
; Name ..........: VarTo2D
; Description ...: Funkcja konwertuje dane tekstowe na tablice, kolejne rekordy to wiersze oddzielone @CRLF
; Syntax ........: VarTo2D($var[, $sSeparator = "|"])
; Parameters ....: $var                 - a variant value.
;                  $sSeparator          - [optional] a string value. Default is "|".
; Return values .: Tablica $aResult
; Author ........: WWW
; ===============================================================================================================================
Func VarTo2D($var, $sSeparator = "|")
    Local $aRows = StringSplit(StringStripCR($var), @CRLF), $aColumns, $aResult[$aRows[0]][1]
    For $iRow = 1 To $aRows[0]
        $aColumns = StringSplit($aRows[$iRow], $sSeparator)
        If $aColumns[0] > UBound($aResult, 2) Then _
			ReDim $aResult[$aRows[0]][$aColumns[0]]
        For $iColumn = 1 To $aColumns[0]
            $aResult[$iRow - 1][$iColumn - 1] = $aColumns[$iColumn]
        Next
    Next
    Return $aResult
EndFunc   ; ==> _VarTo2D()


; #FUNCTION# ====================================================================================================================
; Name ..........: _KoniecOK
; Description ...: Funkcja kończy działanie skryptu zapisując dodatkowe informacje do loga i result.json
; Syntax ........: _KoniecOK([$status = ""[, $result = ""]])
; Parameters ....: $status              - [opcjonalna] wartość "status" w result.json.
;                  $result              - [opcjonalna] wartość "result" w result.json.
; Return values .: None
; Author ........: Piotr Wiśniewski, Mateusz Kłosowski © makeitright.pl
; ===============================================================================================================================
Func _KoniecOK($result="")
	$timeend = _NowCalc()
	$logger_status = "success"
	_Logger("Koniec skryptu: " & $timeend, "br")
	_Logger("Czas działania skryptu: " & _DateDiff('s', $timestart, $timeend) & " sek. [" & StringRight("0" & _DateDiff('h', $timestart, $timeend), 2) & ":" & _
			StringRight("0" & Mod(_DateDiff('n', $timestart, $timeend),60),2) & ":" & StringRight("0" & Mod(_DateDiff('s', $timestart, $timeend),60),2)& "]", "br")
	; opcjonalne wysłanie mailingu
		If IsArray($mailingData) And UBound($mailingData) >= 7 Then
			Do
				; wygenerowanie tytułu emaila
				Local $subject = Call($mailingData[3], Default, Default, $mailingData[6])
				If @error Then ExitLoop
				; wygenerowanie treści emaila
				Local $body = Call($mailingData[4], Default, Default, $result)
				If @error Then ExitLoop
				; wysłanie emaila
				Call($mailingData[1], $mailingData[0], $mailingData[2], $subject, $body, $mailingData[5])
;~ 				If @error Then ExitLoop
				; opóźnienie w związku z ewentualnymi operacjami MS Outlook
				Sleep(1*1000)
				; wyłączenie MS Outlook
				If Not IsKeyword($mailingData[7]) Or $mailingData[7] <> "" Then _
					Call($mailingData[7], $mailingData[0])
			Until True
		EndIf
	; tryb dołączania treści loga biznesowego do pola 'Wynik' w PF
		If $logBizToPF And FileExists($PlikLogBusiness & "log") Then
			Local $hPlikLogBusiness = FileOpen($PlikLogBusiness & "log", 1)
			$logger_result = FileRead($PlikLogBusiness & "log")
			$logger_result = StringRegExpReplace($logger_result, "(\n)+|(\r\n)+|(\r)+|\n+", "\\n")
			$logger_result = StringReplace(StringReplace($logger_result, "\", "\\"), "\\n", "\n")
			$logger_result = "Usługa zakończona ze statusem PASS: szczegóły znajdują się w PowerFarmer, w zakładce 'Informacje techniczne' lub w załączniku do e-mail.\n" & _
				(($result == "" Or IsKeyword($result)) ? "" : $result & "\n") & $logger_result
			FileClose($hPlikLogBusiness)
		ElseIf $result = "" Or IsKeyword($result) Then
			$logger_result = "Usługa zakończona ze statusem PASS: szczegóły znajdują się w PowerFarmer, w zakładce 'Informacje techniczne' lub w załączniku do e-mail."
		Else
			$logger_result &= $result
		EndIf
	; utwórz plik result.json
		_Result()
	ConsoleWrite("+ That's all OK folks :)" & @CRLF)
	Exit
EndFunc   ; ==> _KoniecOK()


; #FUNCTION# ====================================================================================================================
; Name ..........: _KoniecBlad
; Description ...: Funkcja kończy działanie skryptu zapisując dodatkowe informacje do loga i result.json
; Syntax ........: _KoniecBlad($info[, $stack_trace = ""[, $caused_by = ""[, $exit = True]]])
; Parameters ....: $info                - opis powodu błędu w pliku log.
;                  $stack_trace         - [opcjonalna] wartość "stack_trace" w result.json.
;                  $caused_by           - [opcjonalna] wartość "caused_by" w result.json.
;                  $exit				- [opcjonalna] flaga czy zakończyć skrypt
; Return values .: None
; Author ........: Piotr Wiśniewski, Mateusz Kłosowski © makeitright.pl
; ===============================================================================================================================
Func _KoniecBlad($info, $stack_trace="", $caused_by="", $exit=True)
	$logger_status = "failure"
	Local $screenShotFileName = ""
	; dołączenie komunikatu do loga
		If IsKeyword($info) = 0 And $info <> "" Then _
			_LoggerError($info, $stack_trace, $caused_by, "br")
	; wykonanie zrzutu ekranowego aplikacji
		If Not IsKeyword($application) Then _
			$screenShotFileName = _AppScreenShot($application) & ";"
	; opcjonalne wysłanie mailingu
		If IsArray($mailingData) And UBound($mailingData) >= 7 Then
			Do
				; wygenerowanie tytułu emaila
				Local $subject = Call($mailingData[3], Default, Default, $mailingData[6])
				If @error Then ExitLoop
				; wygenerowanie treści emaila
				Local $body = Call($mailingData[4], Default, Default, $info)
				If @error Then ExitLoop
				; wysyłanie emaila z wygenerowanym zrzutem ekranowym
				Call($mailingData[1], $mailingData[0], $mailingData[2], $subject, $body, $screenShotFileName & $mailingData[5] & $mailingData[5])
;~ 				If @error Then ExitLoop
				; opóźnienie w związku z ewentualnymi operacjami MS Outlook
				Sleep(1*1000)
				; wyłączenie MS Outlook
				If Not IsKeyword($mailingData[7]) Or $mailingData[7] <> "" Then _
					Call($mailingData[7], $mailingData[0])
				; usunięcie screenshot'a ze względu na rodo
				If Not $rodoLogging And $screenShotFileName <> "" Then _
					FileDelete($screenShotFileName)
			Until True
		EndIf
	; tryb dołączania treści loga biznesowego do pola 'Wynik' w PF i e-mail (sterowanie flagą globalną $logBizToPF)
		If $logBizToPF And FileExists($PlikLogBusiness & "log") Then
			Local $hPlikLogBusiness = FileOpen($PlikLogBusiness & "log", 1)
			$logger_result = FileRead($PlikLogBusiness & "log")
			$logger_result = StringRegExpReplace($logger_result, "(\n)+|(\r\n)+|(\r)+|\n+", "\\n")
			$logger_result = StringReplace(StringReplace($logger_result, "\", "\\"), "\\n", "\n")
			$logger_result = "Usługa zakończona ze statusem FAIL: szczegóły znajdują się w PowerFarmer, w zakładce 'Informacje techniczne' lub w załączniku do e-mail.\n" & $logger_result
			FileClose($hPlikLogBusiness)
		Else
			$logger_result = "Usługa zakończona ze statusem FAIL: szczegóły znajdują się w PowerFarmer, w zakładce 'Informacje techniczne' lub w załączniku do e-mail."
		EndIf
		If $stack_trace <> "" Then _
			$a_logger_errors[0][1] = $stack_trace ; stack_trace
		If $caused_by <> "" Then _
			$a_logger_errors[1][1] = $caused_by ; caused_by
		$a_logger_errors[2][1] = $screenShotFileName = "" ? "" : StringReplace($PlikLog, "\", "\\") & "jpg" ; screenshot
	; utwórz plik result.json
		_Result()
	If $exit Then _
		Exit
	Return $info
EndFunc   ; ==> _KoniecBlad()


; #FUNCTION# ====================================================================================================================
; Name ..........: _Result
; Description ...: Funkcja tworzy plik result.json na dysku workera
; Syntax ........: _Result()
; Parameters ....: None
; Return values .: None
; Author ........: Piotr Wiśniewski © makeitright.pl
; ===============================================================================================================================
Func _Result()
	Local $wynik = '{"status":"' & $logger_status & '","result":"' & $logger_result &'","errors":{'

	If $a_logger_errors[0][1] <> "" Then
		For $i = 0 To UBound($a_logger_errors,$UBOUND_ROWS)-1
			$wynik = $wynik & '"' & $a_logger_errors[$i][0] & '":"' & $a_logger_errors[$i][1]& '",'
		Next
		If StringRight($wynik,1)="," Then _
			$wynik = StringTrimRight($wynik,1)
	EndIf

	$wynik = $wynik & '}' ; zamknięcie nawiasu dla "errors"

	If UBound($a_logger_returns,$UBOUND_ROWS) > 0 Then
		$wynik = $wynik &'","returns":{' ; początek sekcji "returns"
		For $i = 0 To UBound($a_logger_returns,$UBOUND_ROWS)-1
			$wynik = $wynik & '"' & $a_logger_returns[$i][0] & '":"' & $a_logger_returns[$i][1]& '",'
		Next
		If StringRight($wynik,1)="," Then _
			$wynik = StringTrimRight($wynik,1)
		$wynik = $wynik & '}' ; zamknięcie nawiasu dla "returns"
	EndIf
	$wynik = $wynik & '}' ; zamknięcie nawiasu dla całego json
	Local $Plik = FileOpen($Katalog_workspace & "result\result.json",258)
	FileWrite($Plik, $wynik)
	FileClose($Plik)
EndFunc		; ==> _Result()


; #FUNCTION# ====================================================================================================================
; Name ..........: _Logger
; Description ...: Zapisywanie danych do loga technicznego, biznesowego i/lub bezplikowego
; Syntax ........: _Logger($komunikat[, $log = ""])
; Parameters ....:	$komunikat		- treść zapisywana do pliku log
;                 	$log			- [OPCJA] sterowanie gdzie dodatkowo ma być zalogowany komunikat, kody sterujące można łączyć, np. "rb"
;						"b" = treść zapisywana do pliku loga biznesowego,
;						"n" = treść zapisywana tylko do pliku loga biznesowego,
;						"r" = treść jest zapisywana w pamięci komputera (ze względu na RODO)
; Return values .: treść $komunikat
; Author ........: Piotr Wiśniewski, Mateusz Kłosowski © makeitright.pl
; ===============================================================================================================================
Func _Logger($komunikat, $log = "")

	; sprawdzenie czy komunikat ma lecieć wyłącznie do loga bezplikowego
	If (Not ($log == "r")) And (StringInStr($log, "n") = 0) Then
		If NOT FileExists($PlikLog & "log") Then _
			FileOpen($PlikLog & "log", 258) ; jeżeli plik nie istnieje to zostanie utworzony
		Local $PlikLogAll = FileOpen($PlikLog & "log", 1)
		FileWriteLine($PlikLogAll,@CRLF & "[" & _NowTime() & "] " & $komunikat)
		FileClose($PlikLogAll)
	EndIf
	; komunikat leci TEŻ do loga business
	If StringInStr($log, "b") Or StringInStr($log, "n") Then
		If NOT FileExists($PlikLogBusiness & "log") Then _
			FileOpen($PlikLogBusiness & "log", 258) ; jeżeli plik nie istnieje to zostanie utworzony
		Local $hPlikLogBusiness = FileOpen($PlikLogBusiness & "log",1)
		FileWriteLine($hPlikLogBusiness,@CRLF & "[" & _NowTime() & "] " & $komunikat)
		FileClose($hPlikLogBusiness)
	EndIf
	; komunikat leci m.in. do loga bezplikowego
	If StringInStr($log, "r") And Not StringInStr($log, "n") Then
		If $nofileLogResultIndex >= UBound($nofileLogResultEntries) - 1 Then _
			ReDim $nofileLogResultEntries[UBound($nofileLogResultEntries) + 99]
		$nofileLogResultEntries[$nofileLogResultIndex] = "[" & _NowTime() & "] " & $komunikat
		$nofileLogResultIndex += 1
	EndIf
	Return $komunikat
EndFunc   ; ==> _Logger()


; #FUNCTION# ====================================================================================================================
; Name ..........: _LoggerError
; Description ...: Formatowanie i zapisywanie danych błędu do loga technicznego, biznesowego i/lub bezplikowego
; Syntax ........: _LoggerError($message, $stack_trace[, $caused_by=Default][, $log="br"])
; Parameters ....:	$message	- treść zapisywana do pliku log
;					$caused_by	- [OPCJA] funkcja, która spowodowała błąd
;                 	$log		- [OPCJA] sterowanie gdzie dodatkowo ma być zalogowany komunikat, kody sterujące można łączyć, np. "rb"
;						"b" = treść zapisywana do pliku loga biznesowego,
;						"n" = treść zapisywana tylko do pliku loga biznesowego,
;						"r" = treść jest zapisywana w pamięci komputera (ze względu na RODO)
; Return values .: sformatowana treść komunikatu błędu (bez prefixu 'ERROR')
; Author ........: Mateusz Kłosowski © makeitright.pl
; ===============================================================================================================================
Func _LoggerError($message, $stack_trace, $caused_by=Default, $log="br")
	Local $infoTech, $infoBiz
	; przygotowanie treści komunikatów
		$infoTech = "Komunikat błędu: " & $message
		If Not IsKeyword($caused_by) And $caused_by <> "" Then _
			$infoTech = $infoTech & @CRLF & " Przyczyna błędu: " & $caused_by
		$infoBiz = $infoTech
		If $stack_trace <> "" Then _
			$infoTech = "Podczas działania funkcji '" & $stack_trace & "' wystąpił błąd. " & @CRLF & $infoTech
		If IsKeyword($log) Then $log = ""
	; wysyłka do Loggera
		If $log == "r" Then
			_Logger("[BŁĄD] : " & $infoBiz, "r")
		Else
			If StringInStr($log, "r") Then _
				_Logger("[BŁĄD] : " & $infoBiz, "r")
			If StringInStr($log, "b") Or StringInStr($log, "n") Then _
				_Logger("[BŁĄD] : " & $infoBiz, "n")
			If StringInStr($log, "n") = 0 Then _
				_Logger("[BŁĄD] : " & $infoTech, "")
		EndIf
	Return $infoTech
EndFunc   ; ==> _LoggerError()


; #FUNCTION# ====================================================================================================================
; Name ..........: _AppScreenShot
; Description ...: Wykonanie zrzutu ekranowego aplikacji.
; Syntax ........: _AppScreenShot([$appHWND = Default])
; Parameters ....: $appHWND		- [optional] uchwyt aplikacji, jeśli brak brany jest domyślny ze zmiennej globalnej $application
; Return values .:	ścieżka z nazwą pliku screenshot'a (w folderze result)
;					obiekt @error w przypadku błędu z pustym stringiem
; Author ........: Mateusz Kłosowski © makeitright.pl
; ===============================================================================================================================
Func _AppScreenShot($appHWND=Default)
	If IsKeyword($appHWND) Then
		If IsKeyword($application) Then
			_LoggerError("Zrzut ekranowy nie został wykonany - brak uchwytu do aplikacji.", @ScriptName & " / standard / _AppScreenShot()")
			Return(SetError(1, 0, ""))
		EndIf
		$appHWND = $application
	EndIf
	Local $ssFileName = StringLeft($PlikLog, StringLen($PlikLog)-1) & "_" & StringRegExpReplace(_NowCalc(), ":|/|\h+", "") & ".jpg"
	_ScreenCapture_CaptureWnd($ssFileName, $appHWND) ; wykonuje PrintScreen wskazanego okna aplikacji do katalogu POWERFARMER\workspace\result\attachments
	If @error Then
		_LoggerError("Błąd podczas wykonywania zrzutu ekranowego i próby zapisu do pliku: " & $ssFileName, @ScriptName & " / standard / _AppScreenShot()", "Kod błędu: " & @error)
		Return SetError(2, 0, "")
	EndIf
	_Logger("Wykonano zrzut ekranowy do pliku: " & $ssFileName)
	Return SetError(0, 0, $ssFileName)
EndFunc		; ==> _AppScreenShot()


; #FUNCTION# ====================================================================================================================
; Name ..........: _Msg
; Description ...: Funkcja wyświetlająca okienko komunikatu z przyciskami [TAK] i [NIE] ułatwia analizę skryptu i przerwanie działania skryptu
; Syntax ........: _Msg($naglowek, $tresc)
; Parameters ....: $naglowek            - a general number value.
;                  $tresc               - a dll struct value.
; Return values .: None
; Author ........: Piotr Wiśniewski © makeitright.pl
; ===============================================================================================================================
Func _Msg($naglowek="",$tresc="")
	$tresc = $tresc & @CRLF & @CRLF & "[TAK] - wykonano i lecimy dalej" & @CRLF & "[NIE] - PRZERWANIE działania programu"
	If Not IsDeclared("iMsgBoxAnswer") Then Local $iMsgBoxAnswer
	$iMsgBoxAnswer = MsgBox(52,$naglowek,$tresc)
	Select
		Case $iMsgBoxAnswer = 6 ;Yes

		Case $iMsgBoxAnswer = 7 ;No
			Exit
	EndSelect
EndFunc   ; ==> _Msg()


; #FUNCTION# ====================================================================================================================
; Name ..........: _ZaladujArgumentsOptions
; Description ...: Funkcja odczytuje danych z wskazanego pliku do tablicy.
;						UWAGA: parsowany jest tylko pierwszy poziom json'a (!!!)
; Syntax ........: _ZaladujArgumentsOptions($parametr)
; Parameters ....: $parametr            - "arguments" lub "options"
; Return values .: Tablica z zawartością odczytaną z pliku arguments.json lub parametr.json
; Author ........: Piotr Wiśniewski, Mateusz Kłosowski © makeitright.pl
; ===============================================================================================================================
Func _ZaladujArgumentsOptions($parametr)
	If $parametr <> "arguments" And $parametr <> "options" Then _
		_KoniecBlad("Wywołano funkcję pmfs / _ZaladujArgumentsOptions($parametr) z błędnym parametrem: " & $parametr, _
				"pmfs / _ZaladujArgumentsOptions($parametr)", "Błędny parametr w wywołaniu funkcji")
	If FileExists ($Katalog_workspace & $parametr & ".json") Then
		Local $Plik = FileOpen($Katalog_workspace & $parametr & ".json",0)
		If @error = -1 Then _
			_KoniecBlad("Funkcja _ZaladujArgumentsOptions($parametr) Nie moze otworzyć pliku na dysku: " & $Katalog_workspace & $parametr & ".json")
		Local $odczytany_string = FileRead($Plik) ; odczytujemy plik do stringa
		FileClose($Plik) ; zamykamy plik
		Local $json = Json_Decode($odczytany_string)
		Local $oKeys = Json_ObjGetKeys($json)

		Local $oSubJson, $oSubKeys, $sNameMainKey, $sValueMainKey, $sNameSubKey, $sValueSubKey
		For $i = 0 to Ubound($oKeys)-1
			$sNameMainKey = $oKeys[$i]
			$sValueMainKey = Json_Get($json, "." & $oKeys[$i])
			$oSubJson = Json_ObjGet($json, $oKeys[$i])
			If Json_IsObject($oSubJson) Then	;klucz ma podklucze
				$oSubKeys = Json_ObjGetKeys($oSubJson)		; pobieramy podklucze
				For $ii = 0 to Ubound($oSubKeys) -1
					$sNameSubKey = $oSubKeys[$ii]
					$sValueSubKey = Json_ObjGet($oSubJson, $sNameSubKey)
					If $parametr = "options" Then
						ReDim $a_options[Ubound($a_options, $UBOUND_ROWS)+1][2]
						$a_options[Ubound($a_options, $UBOUND_ROWS)-1][0] = $sNameMainKey & "|" & $sNameSubKey ;nazwa klucza opcji
						$a_options[Ubound($a_options, $UBOUND_ROWS)-1][1] = $sValueSubKey	; wartość opcji
					ElseIf $parametr = "arguments" Then
						ReDim $a_arguments[Ubound($a_arguments, $UBOUND_ROWS)+1][2]
						$a_arguments[Ubound($a_arguments, $UBOUND_ROWS)-1][0] = $sNameMainKey & "|" & $sNameSubKey ;nazwa klucza opcji
						$a_arguments[Ubound($a_arguments, $UBOUND_ROWS)-1][1] = $sValueSubKey	; wartość opcji
					EndIf
				Next
			Else
				If $parametr = "options" Then
					ReDim $a_options[Ubound($a_options, $UBOUND_ROWS)+1][2]
					$a_options[Ubound($a_options, $UBOUND_ROWS)-1][0] = $sNameMainKey ;nazwa klucza opcji
					$a_options[Ubound($a_options, $UBOUND_ROWS)-1][1] = $sValueMainKey	; wartość opcji
				ElseIf $parametr = "arguments" Then
					ReDim $a_arguments[Ubound($a_arguments, $UBOUND_ROWS)+1][2]
					$a_arguments[Ubound($a_arguments, $UBOUND_ROWS)-1][0] = $sNameMainKey ;nazwa klucza opcji
					$a_arguments[Ubound($a_arguments, $UBOUND_ROWS)-1][1] = $sValueMainKey	; wartość opcji
				EndIf
			EndIf
		Next
	EndIf
EndFunc   ; ==> _ZaladujArgumentsOptions()


; #FUNCTION# ====================================================================================================================
; Name ..........: _OdczytajParametr
; Description ...: Funkcja zwraca wartość wywołanego parametru z parametr.json
; Syntax ........: _OdczytajParametr($parametr)
; Parameters ....: $parametr            - nazwa parametru którego chcemy odczytać wartość
; Return values .: string
; Author ........: Piotr Wiśniewski © makeitright.pl
; ===============================================================================================================================
Func _OdczytajParametr($parametr)
	Local $licznik = _ArraySearch($a_options,$parametr)
	If $licznik > -1 Then
		Return $a_options[$licznik][1]
	Else
		Return ""
	EndIf
EndFunc   ; ==> _OdczytajParametr()


; #FUNCTION# ====================================================================================================================
; Name ..........: _OdczytajArgument
; Description ...: Funkcja zwraca wartość wywołanego argumentu z arguments.json
; Syntax ........: _OdczytajArgument($argument)
; Parameters ....: $argument            - nazwa argumentu którego chcemy odczytać wartość
; Return values .: string
; Author ........: Piotr Wiśniewski © makeitright.pl
; ===============================================================================================================================
Func _OdczytajArgument($argument)
	Local $licznik = _ArraySearch($a_arguments,$argument)
	If $licznik > -1 Then
		Return $a_arguments[$licznik][1]
	Else
		Return ""
	EndIf
EndFunc   ; ==> _OdczytajArgument()


; #FUNCTION# ====================================================================================================================
; Name ..........: _GetHwndFromPID
; Description ...: Funkcja pobiera hWnd okna z PID'u w określonym czasie
; Syntax ........: _GetHwndFromPID($PID[, $timeout = 10])
; Parameters ....: $PID                 - PID uruchomionej aplikacji.
;                  $timeout             - czas w którym funkcja stara się pobrać hWND
; Return values .: hWND
; Author ........: WWW
; ===============================================================================================================================
Func _GetHwndFromPID($PID, $timeout = 10)
	Local $secs = TimerInit()
	Local $hWnd = 0
	Local $winlist = WinList()
	Do
		For $i = 1 To $winlist[0][0]
			If $winlist[$i][0] <> "" Then
				Local $iPID2 = WinGetProcess($winlist[$i][1])
				If $iPID2 = $PID Then
					$hWnd = $winlist[$i][1]
					ExitLoop
				EndIf
			EndIf
		Next
	Until ($hWnd <> 0) Or (TimerDiff($secs) >= $timeout * 1000) ;kończy pętlę po znalezieniu hWnd lub czasie timeout w sekundach
	Return $hWnd
EndFunc	; ==> _GetHwndFromPID()


; #FUNCTION# ====================================================================================================================
; Name ..........: _GetHwndFromString
; Description ...: Funkcja pobiera hWnd okna w oparciu o nazwę okna w określonym czasie
; Syntax ........: _GetHwndFromString($windowName[, $timeout = 10])
; Parameters ....: $WindowName          - nazwa okna uruchomionej aplikacji.
;                  $timeout             - czas w którym funkcja stara się pobrać hWND
; Return values .: hWND
; Author ........: Rafał Warszycki MIR
; ===============================================================================================================================
Func _GetHwndFromString($windowName, $timeout = 10)
	Local $secs = TimerInit()
	Local $hWnd = 0
	Local $i = 0
	Do
		Local $winlist = WinList()
		If $winlist[$i][0] <> "" Then
			If StringInStr($winlist[$i][0], $windowName) > 0 Then
				$hWnd = $winlist[$i][1]
				ExitLoop
			EndIf
		EndIf
		$i = $i + 1
		If $i == UBound($winlist, 1) Then
			$i = 0
		EndIf
	Until ($hWnd <> 0) Or (TimerDiff($secs) >= $timeout * 1000) ;kończy pętlę po znalezieniu hWnd lub czasie timeout w sekundach
	Return $hWnd
EndFunc	; ==> _GetHwndFromString()



; #FUNCTION# ====================================================================================================================
; Name ..........: _Info
; Description ...: Funkcja de debugowania skryptów, wyświetla po lewej stronie SplashText który jest aktualizowany z kadym wywołaniem funkcji
; Syntax ........: _Info($tresc)
; Parameters ....: $tresc               - string o składni "[treść]=[wartość];[treść_n]=[wartość_n]"
; Return values .: None
; Author ........: Piotr Wiśniewski © makeitright.pl
; ===============================================================================================================================
Func _Info($tresc)
	$tresc = StringReplace($tresc,"=",": ")
	$tresc = StringReplace($tresc,";",@CRLF)

	If NOT WinExists(@ScriptName) Then
		Local $VirtX = DllCall("user32.dll", "int", "GetSystemMetrics", "int", 78)
		Sleep(500)
		If Not IsKeyword($application) Then _
			SplashTextOn(@ScriptName,$tresc,205,600,(($VirtX[0]-15)-(WinGetClientSize($application,"")[0])-220),5,4,"Courier New",9)
	Else
		ControlSetText(@ScriptName,"","Static1",$tresc)
	EndIf
EndFunc		; ==> _Info()


; #FUNCTION# ====================================================================================================================
; Name ..........: _LiczbaPoPrzecinku
; Description ...: Obcina string do wskazanych miejsc po przecinku
; Syntax ........: _LiczbaPoPrzecinku($liczba[, $miejsca = "2"])
; Parameters ....: $liczba - dowolny string zawierający TYLKO 1 przecinek
;                  $miejsca - [optional] a map. Default is "2".
; Return values .: Nowa wartość lub False
; Author ........:  Piotr Wiśniewski © makeitright.pl
; ===============================================================================================================================
Func _LiczbaPoPrzecinku($liczba, $miejsca="2")
	If $liczba <> "" Then
		StringReplace($liczba, ",", ",")
		If @extended > 1 Then
			Return False
		ElseIf @extended = 0 Then
			Return $liczba
		EndIf
		Local $PozycjaPrzecinka = StringInStr($liczba,",")
		If $PozycjaPrzecinka > StringLen(StringTrimLeft($liczba,$PozycjaPrzecinka)) Then _
			Return False
		If StringLen($liczba) - $PozycjaPrzecinka >= $miejsca Then _
			Return StringLeft($liczba, $PozycjaPrzecinka + $miejsca)
	Else
		Return $liczba
	EndIf
EndFunc		; ==> _LiczbaPoPrzecinku()


; #FUNCTION# ====================================================================================================================
; Name ..........: _WalidujDate
; Description ...: Walidacja czy podany string jest w określonym w argumencie formacie daty
; Syntax ........: _WalidujDate($dataDoWalidacji, $formatDaty = "yyyy-MM-dd")
; Parameters ....: $dataDoWalidacji     - string do walidacji czy jest datą w odpowiednim formacie
;				   $formatDaty			- walidowany format daty, domyślnie "yyyy-MM-dd"
; Return values .: Data w formacie określonym w argumencie $formatDaty.
;					Jeżeli jest błąd ustawiana jest wartość @error i zwracany komunikat błędu.
; Author ........: Mateusz Kłosowski © makeitright.pl
; ===============================================================================================================================
Func _WalidujDate($dataDoWalidacji, $formatDaty = "yyyy-MM-dd")
	Local $komunikatBleduWalidacji = "Nieprawidłowy format podanej daty (" & $dataDoWalidacji & "): "
	Local $data = StringStripWS($dataDoWalidacji, 8)
	Switch $formatDaty
		Case "yyyy-MM-dd"
;~ 			Local $aDataStanu = StringSplit(StringReplace(StringReplace(StringReplace(StringReplace($data,":","-"),".","-"),"/","-"), "\", "-"), "-")
			$data = StringRegExpReplace($data, ":+|\.+|/+|\\+", "-")
			Local $aDataStanu = StringSplit($data, "-")
			Select
				Case StringLen($data) = 0
					Return SetError(2, 0, $komunikatBleduWalidacji & "brak podanej daty (oczekiwano daty w formacie 'yyyy-MM-dd').")
				Case StringLen($data) <> 10
					Return SetError(3, 0, $komunikatBleduWalidacji & "oczekiwano 10 znaków (data powinna być w formacie 'yyyy-MM-dd').")
				Case $aDataStanu[0] < 3
					Return SetError(4, 0, $komunikatBleduWalidacji & "data powinna być w formacie 'yyyy-MM-dd'.")
				Case Not (StringIsDigit($aDataStanu[1]) And StringIsDigit($aDataStanu[2]) And StringIsDigit($aDataStanu[3]))
					Return SetError(5, 0, $komunikatBleduWalidacji & "data zawiera nieprawidłowe znaki - obsługiwane są tylko cyfry i separator '-'.")
				Case StringLen($aDataStanu[1]) < 4
					Return SetError(6, 0, $komunikatBleduWalidacji & "rok powinien być 4-cyfrowy (data powinna być w formacie 'yyyy-MM-dd').")
				Case StringLen($aDataStanu[2]) < 2
					Return SetError(7, 0, $komunikatBleduWalidacji & "miesiąc powinien być 2-cyfrowy, z poprzedzającym zerem dla miesięcy 1-9 (data powinna być w formacie 'yyyy-MM-dd').")
				Case StringLen($aDataStanu[3]) < 2
					Return SetError(8, 0, $komunikatBleduWalidacji & "dzień powinien być 2-cyfrowy, z poprzedzającym zerem dla dni 1-9 (data powinna być w formacie 'yyyy-MM-dd').")
				Case Number($aDataStanu[1]) < 1900 Or Number($aDataStanu[1]) > @YEAR
					Return SetError(9, 0, $komunikatBleduWalidacji & "rok mniejszy niz 1900 i większy niż aktualny (" & @YEAR & ").")
				Case Number($aDataStanu[2]) < 1 Or Number($aDataStanu[2]) > 12
					Return SetError(10, 0, $komunikatBleduWalidacji & "miesiąc mniejszy niż 1 i większy niż 12.")
				Case Number($aDataStanu[3]) < 1 Or _DateDaysInMonth(Number($aDataStanu[1]), Number($aDataStanu[2])) < Number($aDataStanu[3])
					Return SetError(11, 0, $komunikatBleduWalidacji & "nieprawidłowa liczba dni w miesiącu.")
			EndSelect
		Case Else
			Return SetError(1, 0, "Nieprawidłowy argument funkcji walidującej format daty.")
	EndSwitch
	; walidacja bezbłędna
	Return $data
EndFunc		; ==> _WalidujDate()


; #FUNCTION# ====================================================================================================================
; Name ..........: _ModArray
; Description ...: Ustawienie wartości będącej w podanej tablicy w podanym indexie. Funkcja przydatna gdy trzeba zmodyfikować wartość w tablicy, która jest wewnątrz innej tablicy.
; Syntax ........: _ModArray($aArray, $iIndex, $value)
; Parameters ....: $aArray	- tablica do zmodyfikowania
;                  $iIndex	- index w tablicy
;                  $value	- nowa wartość
; Return values .: None
; Author ........: Mateusz Kłosowski © Makeitright
; ===============================================================================================================================
Func _ModArray(ByRef $aArray, $iIndex, $value)
	$aArray[$iIndex] = $value
EndFunc		; ==> _ModArray()


; #FUNCTION# ====================================================================================================================
; Name ..........: _GetUserFullName
; Description ...: Pobranie pełnej nazwy zalogowanego na stacji użytkownika z katalogu AD.
; Syntax ........: _GetUserFullName()
; Parameters ....: None
; Return values .: Nazwa użytkownika lub @error i pusty string.
; Author ........: www
; ===============================================================================================================================
Func _GetUserFullName()
    Local $colItems = ""
    Local $objWMIService = ObjGet("winmgmts:\\localhost\root\CIMV2")
    $colItems = $objWMIService.ExecQuery("SELECT * FROM Win32_UserAccount WHERE Name = '" & @UserName &  "'", "WQL", 0x10 + 0x20)
	If IsObj($colItems) Then
		For $objItem In $colItems
			Return $objItem.FullName
		Next
	Endif
	Return SetError(1, 0, "")
EndFunc		; ==> _GetUserFullName()


; #FUNCTION# ====================================================================================================================
; Name ..........: _DeCrypt
; Description ...: Deszyfrowanie hasła z argumentu używając klucza z pliku.
; Syntax ........: _DeCrypt($haslo)
; Parameters ....:	$sciezkaPlikKey		- ścieżka do pliku z kluczem
;					$haslo				- zaszyfrowane hasło
; Return values .: hasło jawne lub @error z komunikatem błędu
; Author ........: Mateusz Kłosowski © Makeitright
; ===============================================================================================================================
Func _DeCrypt($sciezkaPlikHash, $haslo)
	; biblioteka szyfrująca
	_Crypt_Startup()
	If @error Then _
		Return SetError(1, 0, _LoggerError($sMsgWorkerError, @ScriptName & " / standard / _DeCrypt()", "Błąd uruchamiania biblioteki szyfrowania.", "br"))
	; klucz do deszyfrowania z pliku
	Local $hKey = FileRead($sciezkaPlikHash)
	If @error Then _
		Return SetError(2, 0, _LoggerError($sMsgWorkerError, @ScriptName & " / standard / _DeCrypt()", "Błąd odczytu pliku z kluczem szyfrowania na stacji wykonawczej.", "br"))
	; odszyfrowanie hasła z użyciem klucza z pliku
	Local $hasloOdkodowaneBinary = _Crypt_DecryptData($haslo, $hKey, $CALG_AES_256)
	If @error Then _
		Return SetError(3, 0, _LoggerError($sMsgWorkerError, @ScriptName & " / standard / _DeCrypt()", "Błąd rozszyfrowania hasła.", "br"))
	_Crypt_Shutdown()
	Return BinaryToString($hasloOdkodowaneBinary)
EndFunc		; ==> _DeCrypt()


; #FUNCTION# ====================================================================================================================
; Name ..........: _GetUserFullData
; Description ...: Pobieranie danych zalogowanego użytkownika z ActiveDirectory.
; Syntax ........: _GetUserFullData()
; Parameters ....: None
; Return values .: tablica z odczytanymi danymi: [ 0:email | 1:imię | 2:nazwisko | 3:pełna nazwa ]
; Author ........: www
; ===============================================================================================================================
Func _GetUserFullData()
	$oMyError = ObjEvent("AutoIt.Error", "ComError")
	Local $objRootDSE = ObjGet("LDAP://RootDSE")
	Local $objTrans = ObjCreate("NameTranslate")
	$objTrans.Init (3, "")
	$objTrans.Set (1, @LogonDomain)
	$objTrans.Set (3, @LogonDomain & "\" & @UserName)
	Local $strUserDN = $objTrans.Get (1)
	Local $UserObj = ObjGet("LDAP://" & $strUserDN)
	Local $emailaddress		= $UserObj.EmailAddress
	Local $firstname		= $UserObj.FirstName
	Local $lastname			= $UserObj.LastName
	Local $fullname			= $UserObj.FullName
;~ 	Local $login			= @UserName
;~ 	Local $department		= $UserObj.Department
;~ 	Local $streetaddress	= $UserObj.get("streetAddress")
;~ 	Local $city				= $UserObj.get("l")
;~ 	Local $state			= $UserObj.get("st")
;~ 	Local $zipcode			= $UserObj.PostalCodes
;~ 	Local $country			= $UserObj.get("c")
;~ 	Local $officenumber		= $UserObj.TelephoneNumber
;~ 	Local $mobilenumber		= $UserObj.TelephoneMobile
;~ 	Local $faxnumber		= $UserObj.FaxNumber
;~ 	Local $homeMDB			= $UserObj.get("homeMDB")
;~ 	Local $homeMDBtext		= StringSplit($homeMDB, ",")
;~ 	MsgBox(0, "AD", $firstname & " " & $lastname & " [" & $fullname & "]" & " - " & $login &  @CRLF & $streetaddress & @CRLF & $zipcode & " " & $city & " (" & $state & ") " & " - " & $country & @CRLF & $emailaddress)
	Local $return[4] = [ $emailaddress, $firstname, $lastname, $fullname ]
	$oMyError = ObjEvent("AutoIt.Error", "")
	Return $return
EndFunc		; ==> _GetUserFullData()

; COM Error handler dla funkcji _GetUserFullData()
Func ComError()
	If IsObj($oMyError) Then
		Local $HexNumber = Hex($oMyError.number, 8)
		Return SetError($HexNumber, 0, "Błąd pobierania danych")
	EndIf
	Return SetError(1, 0, "Błąd inicjalizacji obiektu")
EndFunc


; #FUNCTION# ====================================================================================================================
; Name ..........: _StringNameCase
; Description ...: Zamiana pierwszych liter w podanym ciągu znaków na wielkie.
;					Funkcja powstała na bazie funkcji _StringTitleCase()
; Syntax ........: _StringNameCase($sString)
; Parameters ....: $sString		- ciąg znaków do konwersji
; Return values .: ciąg znaków sformatowanych "Jak W Nazwie  Własnej"
; Author ........: Mateusz Kłosowski © Makeitright
; ===============================================================================================================================
Func _StringNameCase($sString)
	Local $bCapNext = True, $sChr = "", $sReturn = ""
	$sString = StringLower($sString)
	For $i = 1 To StringLen($sString)
		$sChr = StringMid($sString, $i, 1)
		If $sChr = " " Then
			$bCapNext = True
		ElseIf $bCapNext Then
			$sChr = StringUpper($sChr)
			$bCapNext = False
		EndIf
		$sReturn &= $sChr
	Next
	Return $sReturn
EndFunc		; ==> _StringNameCase()


; #FUNCTION# ====================================================================================================================
; Name ..........: _ReplacePolishCharacters
; Description ...: Zamiana polskiech znaków w podanym stringu.
; Syntax ........: _ReplacePolishCharacters($sString)
; Parameters ....: $sString		- tekst do zmiany
; Return values .: tekst z zamienionymi polskimi znakami.
; Author ........: Mateusz Kłosowski © Makeitright
; ===============================================================================================================================
Func _ReplacePolishCharacters($sString)
	Local $aPLstripUpper[18] = [ "Ą", "A", "Ć", "C", "Ę", "E", "Ł", "L", "Ń", "N", "Ó", "O", "Ś", "S", "Ź", "Z", "Ż", "Z" ]
	Local $aPLstripLower[18] = [ "ą", "a", "ć", "c", "ę", "e", "ł", "l", "ń", "n", "ó", "o", "ś", "s", "ź", "z", "ż", "z" ]
	For $i = 0 To UBound($aPLstripUpper) -1 Step 2
		$sString = StringReplace($sString, $aPLstripUpper[$i], $aPLstripUpper[$i+1], 0, $STR_CASESENSE)
	Next
	For $i = 0 To UBound($aPLstripLower) -1 Step 2
		$sString = StringReplace($sString, $aPLstripLower[$i], $aPLstripLower[$i+1], 0, $STR_CASESENSE)
	Next
	Return $sString
EndFunc


; #FUNCTION# ====================================================================================================================
; Name ..........: _CW
; Description ...: Wypisanie na konsoli podanego tekstu z przejściem do nowej linii i dodaniem extra linii dla następnego wpisu w konsoli.
; Syntax ........: _CW($msg)
; Parameters ....: $msg		- treść do wpisania na konsolę (kody kolorystyczne: ! red  > blue  - orange  + green)
; Return values .: None
; Author ........: Mateusz Kłosowski © Makeitright
; ===============================================================================================================================
Func _CW($msg)
	ConsoleWrite($msg & @CRLF & @CRLF)
EndFunc


; #FUNCTION# ====================================================================================================================
; Name ..........: _SynchronizeWithMain
; Description ...: Przepisanie danych z UDF Main.au3 do zmiennych w standard.au3.
; Syntax ........: _SynchronizeWithMain()
; Parameters ....: None
; Return values .: None
; Author ........: Mateusz Kłosowski © Makeitright
; ===============================================================================================================================
Func _SynchronizeWithMain()
	$Main_G_FolderWorkspace = $Katalog_workspace
	$Main_G_FileLog = $PlikLog & "log"
	$Main_G_FileLogBusiness = $PlikLogBusiness & "log"
	$Main_G_LoggerResult = $logger_result
	$Main_G_LoggerStatus = $logger_status
	$Main_G_TimeStart = $timestart
	$Main_G_LogBizToPF = $logBizToPF
	$Main_G_aOptions = $a_options
	$Main_G_aArguments = $a_arguments
	$Main_G_MailingData = $mailingData
EndFunc



;=========================================================
;=========================================================
; SEKCJA TESTOWEGO URUCHAMIANIA FUNKCJI
;=========================================================
If @ScriptName = "standard.au3" Then
	_Msg("UWAGA! Wywołanie z FUNKCJI","Uruchomiono skrypt wywołując z funkcji: " & @ScriptName)
	$Katalog_workspace = StringLeft(@ScriptDir,StringInStr(@ScriptDir,"\workspace\")+10)
	$PlikLog = $Katalog_workspace & StringReplace(StringReplace(StringReplace($timestart,":","")," ",""),"/","") & "_Main."
;==Poniżej można umieszczać kod testowy działania poszczególnych funkcji


EndIf