﻿#AutoIt3Wrapper_Au3Check_Parameters=-d -w 1 -w 2 -w 3 -w 5 -w 6 -w 7
Global $Main_G_GUI = True						; True [default] weryfikacja dostępności środowiska (włączony, zalogowany, dostępny i widoczny pulpit)
Global $Main_G_FolderWorkspace = ""			; ścieżka do katalogu workspace (NIE zawsze będzie C:\POWERFARMER\workspace\)
Global $Main_G_FileLog = ""					; zmienna przechowuje nazwe pliku log, który tworzy się w attachments: {ścieżka_do_workspace}\result\attachments\RRRRMMDDGGHHSS_Main.log
Global $Main_G_FileLogBusiness = ""			; zmienna przechowuje nazwe pliku log, który tworzą się w attachments: {ścieżka_do_workspace}\result\attachments\RRRRMMDDGGHHSS_Main.log

Global $Main_G_LoggerResult = ""				; wynik wartości "result"
Global $Main_G_LoggerStatus = ""				; wynik wartości "status": 'failure' lub 'success'
Global $Main_G_aLoggerReturns[0][2]			; tablica do przechowywania wartości zwracanych przez skrypt w sekcji "returns" [nazwa][wartość]
Global $Main_G_aLoggerErrors[3][2] = [["stack_trace",""],["caused_by",""],["screenshot",""]] ; tablica do przechowywania wartości zwracanych przez skrypt w sekcji "errors" [nazwa][wartość]
Global $Main_G_LoggerWynik = ""				; treść results.json do zapisania do pliku
Global $Main_G_TimeStart = _NowCalc()			; data: czas uruchomienia skryptu
Global $Main_G_TimeEnd	=""						; data: czas zakończenia skryptu
Global $Main_G_Application = Null				; hWnd okna Aplikacji
Global $Main_G_LogBizToPF = False				; flaga czy do pola 'Wynik' w PF i do treści e-mail przekazywać również treść loga biznesowego

Global $Main_G_aOptions[0][2]					; tablica przechowująca dane z pliku options.json
Global $Main_G_aArguments[0][2]				; tablica przechowująca dane z pliku arguments.json
Global $Main_G_aResultReturns[0][2]			; tablica przechowuje dane argumentów zwracane w result.json

Global $Main_G_MailingData = Null				; tablica z danymi do wysyłania mailingu po zakończeniu działania skryptu:
										; [0:uchwyt do klienta mail|1:nazwa funkcji wysyłająca mailing|2: adres email|3:nazwa funkcji generująca tytuł|4:nazwa funkcji generująca treść|5:załączniki rozdzielone ";"|...
										;	6:dodatkowe info w tytule maila|7:nazwa funkcji wyłączająca klienta mail]
										; przykład: [ $oOutlook, "_casepromailing_SendEmail", $OPERATOR_MAIL, "_callcenter_PrepareMailSubject", "_callcenter_PrepareMailContent", $Main_ScreenShotFileName, $Main_iD_UCZESTNIKA ]

Global $Main_G_NoFileLogResultEntries[100]		; wpisy do loga bezplikowego
Global $Main_G_NoFileLogResultIndex = 0		; index kolejnego wpisu loga bezplikowego
Global $Main_G_RodoLogging = True				; czy logować do loggerów plikowych gdy jest uwzględniane rodo

Global $Main_G_sMsgWorkerError = "Błąd techniczny na stacji uruchomieniowej: "		; prefix komunikatu dla Operatora informujący o błędzie na stacji uruchomieniowej

; wyrażenie regularne wykorzystywane do zamiany znaków ["-" | "+" | "." | ";" | ","] wraz z poprzedzającymi/następującymi spacjami na pojedynczy znak "," - wykorzystywane w funkcji StringRegExpReplace()
Global $Main_G_REGEX_REPLACE_LIST = "\h+-+\h+|\h+-+|-+\h+|-+|\h+\.+\h+|\h+\.+|\.+\h+|\.+|\h+;+\h+|\h+;+|;+\h+|;+|\h+,+\h+|\h+,+|,+\h+|,+"



#include-once
#include <Recovery.au3>
#include <Array.au3>
#include <Clipboard.au3>
#include <Crypt.au3>
#include <Date.au3>
#include <File.au3>
#include <FileConstants.au3>
#include <GuiComboBox.au3>
#include <MsgBoxConstants.au3>
#include <ScreenCapture.au3>
#include <SQLite.au3>
#include <String.au3>
#include <Timers.au3>
#include <BinaryCall.au3>
#include <Json.au3>



; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_StartScript
; Description ...: Funkcja wykonuje sekwencję starową skryptów poprzez ustawienie parametrów, zmiennych, etc.
; Syntax ........: _Main_StartScript()
; Return values .: None
; Author ........: Piotr Wiśniewski © makeitright.pl
; Modified ......: 2019-03-12 - dodano rozszerzenie pliku "log" dla $Main_G_FileLog i $Main_G_FileLogBusiness
;                             - zmiana nazw zmiennych na ANG
; ===============================================================================================================================
Func _Main_StartScript()
	HotKeySet("{PAUSE}", "_Main_Stop")
	$Main_G_TimeStart = _NowCalc()
;~ 	$Blad = False ; ustawienie znacznika błędu na Fałsz
;~ 	$BladOpis = ""
	$Main_G_FolderWorkspace = StringLeft(@ScriptDir,StringInStr(@ScriptDir,"\workspace\") + 10)
	$Main_G_FileLog = $Main_G_FolderWorkspace & "result\attachments\" & "Main_" & StringRegExpReplace($Main_G_TimeStart, ":|/|\h+","") & ".log"	;~ 	StringReplace(StringReplace(StringReplace($Main_G_TimeStart,":","")," ",""),"/","") & "."
	$Main_G_FileLogBusiness = $Main_G_FolderWorkspace & "result\attachments\" & "Main_Business_" & StringRegExpReplace($Main_G_TimeStart, ":|/|\h+","") & ".log"
	If FileOpen($Main_G_FileLog, 258) = -1 Then		; create file log Main
		_Main_EndError("_Main_StartScript() - Nie mona utworzyć pliku: " & $Main_G_FileLog, "_Main_StartScript()", "Nie mona utworzyć pliku: " & $Main_G_FileLog, True)
	EndIf
	If FileOpen($Main_G_FileLogBusiness, 258) = -1 Then	; create file log Business
		_Main_EndError("_Main_StartScript() - Nie mona utworzyć pliku: " & $Main_G_FileLogBusiness, "_Main_StartScript()", "Nie mona utworzyć pliku: " & $Main_G_FileLogBusiness, True)
	EndIf
	_Main_Logger("Skrypt: " & @ScriptName, "b")
	_Main_Logger("Start skryptu: " & $Main_G_TimeStart, "br")
	_Main_LoadArgumentsOptions("options") ; odczyt danych z options.json do tablicy $Main_G_aOptions
	_Main_LoadArgumentsOptions("arguments") ; odczyt danych z arguments.json do tablicy $Main_G_aArguments
	If $Main_G_GUI = True Then
		If MouseGetCursor() = -1 Then
			_Main_EndError('Parametr skryptu ustawiony "$C_GUI = True", a stacja workera: ' & @ComputerName & ' jest niedostępna w trybie GUI -brak dostępnego pulpitu.', _
			  @ScriptName, 'Parametr skryptu ustawiony "$C_GUI = True", a stacja workera: ' & @ComputerName & ' jest niedostępna w trybie GUI -brak dostępnego pulpitu.', True)
		EndIf
		If _Main_IsWorkstationLockedModern() = True Then
			_Main_EndError('Parametr skryptu ustawiony  "$C_GUI = True", a stacja workera: ' & @ComputerName & ' jest ZABLOKOWANA -brak dostępnego pulpitu.', _
			  @ScriptName, 'Parametr skryptu ustawiony  "$C_GUI = True", a stacja workera: ' & @ComputerName & ' jest ZABLOKOWANA -brak dostępnego pulpitu.', True)
		EndIf
	EndIf
EndFunc   ; ==> _Main_StartScript()



; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_Stop
; Description ...: Funkcja wykorzystywana do obsłużenia HotKey tzw. "emergency button" zatrzymania skryptu
; Syntax ........: _Main_Stop()
; Parameters ....: None
; Return values .: None
; Author ........: Piotr Wiśniewski © makeitright.pl
; ===============================================================================================================================
Func _Main_Stop()
		_Main_EndError(@ScriptName & " PRZERWANY przez użytkownika", @ScriptName, "Przerwano przez użytkownika")
EndFunc		; ==> _Main_Stop()


; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_EndOK
; Description ...: Funkcja kończy działanie skryptu zapisując dodatkowe informacje do loga i result.json
; Syntax ........: _Main_EndOK([$status = ""[, $result = ""]])
; Parameters ....: $status              - [opcjonalna] wartość "status" w result.json.
;                  $result              - [opcjonalna] wartość "result" w result.json.
; Return values .: None
; Author ........: Piotr Wiśniewski, Mateusz Kłosowski © makeitright.pl
; Modified ......: 2019-03-12 - zmiana nazw zmiennych na ANG
; ===============================================================================================================================
Func _Main_EndOK($result="")
	$Main_G_TimeEnd = _NowCalc()
	$Main_G_LoggerStatus = "success"
	_Main_Logger("Koniec skryptu: " & $Main_G_TimeEnd, "br")
	_Main_Logger("Czas działania skryptu: " & _DateDiff('s', $Main_G_TimeStart, $Main_G_TimeEnd) & " sek. [" & StringRight("0" & _DateDiff('h', $Main_G_TimeStart, $Main_G_TimeEnd), 2) & ":" & _
			StringRight("0" & Mod(_DateDiff('n', $Main_G_TimeStart, $Main_G_TimeEnd),60),2) & ":" & StringRight("0" & Mod(_DateDiff('s', $Main_G_TimeStart, $Main_G_TimeEnd),60),2)& "]", "br")
	; opcjonalne wysłanie mailingu
		If IsArray($Main_G_MailingData) And UBound($Main_G_MailingData) >= 7 Then
			Do
				; wygenerowanie tytułu emaila
				Local $Main_subject = Call($Main_G_MailingData[3], Default, Default, $Main_G_MailingData[6])
				If @error Then ExitLoop
				; wygenerowanie treści emaila
				Local $Main_body = Call($Main_G_MailingData[4], Default, Default, $result)
				If @error Then ExitLoop
				; wysłanie emaila
				Call($Main_G_MailingData[1], $Main_G_MailingData[0], $Main_G_MailingData[2], $Main_subject, $Main_body, $Main_G_MailingData[5])
;~ 				If @error Then ExitLoop
				; opóźnienie w związku z ewentualnymi operacjami MS Outlook
				Sleep(1*1000)
				; wyłączenie MS Outlook
				Call($Main_G_MailingData[7], $Main_G_MailingData[0])
			Until True
		EndIf
	; tryb dołączania treści loga biznesowego do pola 'Wynik' w PF
		If $Main_G_LogBizToPF And FileExists($Main_G_FileLogBusiness) Then
			Local $Main_hPlikLogBusiness = FileOpen($Main_G_FileLogBusiness, 1)
			$Main_G_LoggerResult = FileRead($Main_G_FileLogBusiness)
			$Main_G_LoggerResult = StringRegExpReplace($Main_G_LoggerResult, "(\n)+|(\r\n)+|(\r)+|\n+", "\\n")
			$Main_G_LoggerResult = StringReplace(StringReplace($Main_G_LoggerResult, "\", "\\"), "\\n", "\n")
			$Main_G_LoggerResult = "Usługa zakończona ze statusem PASS: szczegóły znajdują się w PowerFarmer, w zakładce 'Informacje techniczne' lub w załączniku do e-mail.\n" & _
				(($result == "" Or IsKeyword($result)) ? "" : $result & "\n") & $Main_G_LoggerResult
			FileClose($Main_hPlikLogBusiness)
		ElseIf $result = "" Or IsKeyword($result) Then
			$Main_G_LoggerResult = "Usługa zakończona ze statusem PASS: szczegóły znajdują się w PowerFarmer, w zakładce 'Informacje techniczne' lub w załączniku do e-mail."
		Else
			$Main_G_LoggerResult &= $result
		EndIf
	; utwórz plik result.json
		_Main_Result()
	;ConsoleWrite("+ That's all OK folks :)" & @CRLF)
	Exit
EndFunc   ; ==> _Main_EndOK()


; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_EndError
; Description ...: Funkcja kończy działanie skryptu zapisując dodatkowe informacje do loga i result.json
; Syntax ........: _Main_EndError($Main_info[, $Main_Stack_Trace = ""[, $Main_CausedBy = ""[, $exit = True]]])
; Parameters ....: $Main_info                - opis powodu błędu w pliku log.
;                  $Main_Stack_Trace         - [opcjonalna] wartość "stack_trace" w result.json.
;                  $Main_CausedBy           - [opcjonalna] wartość "caused_by" w result.json.
;                  $exit				- [opcjonalna] flaga czy zakończyć skrypt
; Return values .: None
; Author ........: Piotr Wiśniewski, Mateusz Kłosowski © makeitright.pl
; Modified ......: 2019-03-12 - zmiana nazw zmiennych na ANG
; ===============================================================================================================================
Func _Main_EndError($Main_info, $Main_Stack_Trace="", $Main_CausedBy="", $exit=True)
	$Main_G_LoggerStatus = "failure"
	Local $Main_ScreenShotFileName = ""
	; dołączenie komunikatu do loga
		If IsKeyword($Main_info) = 0 And $Main_info <> "" Then _
			_Main_LoggerError($Main_info, $Main_Stack_Trace, $Main_CausedBy, "br")
	; wykonanie zrzutu ekranowego aplikacji
		If Not IsKeyword($Main_G_Application) Then
			_ScreenCapture_CaptureWnd(StringTrimRight($Main_G_FileLog, 3) & "png", $Main_G_Application) ; wykonuje PrintScreen wskazanego okna aplikacji do katalogu POWERFARMER\workspace\result\attachments
			If @error Then
				_Main_Logger("Błąd podczas wykonywania zrzutu ekranowego.")
			Else
				$Main_ScreenShotFileName = StringTrimRight($Main_G_FileLog, 3) & "png;"
			EndIf
		EndIf
	; opcjonalne wysłanie mailingu
		If IsArray($Main_G_MailingData) And UBound($Main_G_MailingData) >= 7 Then
			Do
				; wygenerowanie tytułu emaila
				Local $Main_subject = Call($Main_G_MailingData[3], Default, Default, $Main_G_MailingData[6])
				If @error Then ExitLoop
				; wygenerowanie treści emaila
				Local $Main_body = Call($Main_G_MailingData[4], Default, Default, $Main_info)
				If @error Then ExitLoop
				; wysyłanie emaila z wygenerowanym zrzutem ekranowym
				Call($Main_G_MailingData[1], $Main_G_MailingData[0], $Main_G_MailingData[2], $Main_subject, $Main_body, $Main_ScreenShotFileName & $Main_G_MailingData[5])
;~ 				If @error Then ExitLoop
				; opóźnienie w związku z ewentualnymi operacjami MS Outlook
				Sleep(1*1000)
				; wyłączenie MS Outlook
				Call($Main_G_MailingData[7], $Main_G_MailingData[0])
				; usunięcie screenshot'a ze względu na rodo
				If Not $Main_G_RodoLogging And $Main_ScreenShotFileName <> "" Then _
					FileDelete($Main_ScreenShotFileName)
			Until True
		EndIf
	; tryb dołączania treści loga biznesowego do pola 'Wynik' w PF i e-mail (sterowanie flagą globalną $Main_G_LogBizToPF)
		If $Main_G_LogBizToPF And FileExists($Main_G_FileLogBusiness) Then
			Local $Main_hPlikLogBusiness = FileOpen($Main_G_FileLogBusiness, 1)
			$Main_G_LoggerResult = FileRead($Main_G_FileLogBusiness)
			$Main_G_LoggerResult = StringRegExpReplace($Main_G_LoggerResult, "(\n)+|(\r\n)+|(\r)+|\n+", "\\n")
			$Main_G_LoggerResult = StringReplace(StringReplace($Main_G_LoggerResult, "\", "\\"), "\\n", "\n")
			$Main_G_LoggerResult = "Usługa zakończona ze statusem FAIL: szczegóły znajdują się w PowerFarmer, w zakładce 'Informacje techniczne' lub w załączniku do e-mail.\n" & $Main_G_LoggerResult
			FileClose($Main_hPlikLogBusiness)
		Else
			$Main_G_LoggerResult = "Usługa zakończona ze statusem FAIL: szczegóły znajdują się w PowerFarmer, w zakładce 'Informacje techniczne' lub w załączniku do e-mail."
		EndIf
		If $Main_Stack_Trace <> "" Then _
			$Main_G_aLoggerErrors[0][1] = $Main_Stack_Trace ; stack_trace
		If $Main_CausedBy <> "" Then _
			$Main_G_aLoggerErrors[1][1] = $Main_CausedBy ; caused_by
		$Main_G_aLoggerErrors[2][1] = $Main_ScreenShotFileName = "" ? "" : StringReplace($Main_G_FileLog, "\", "\\") & "png" ; screenshot
	; utwórz plik result.json
		_Main_Result()
	If $exit Then _
		Exit
	Return $Main_info
EndFunc   ; ==> _Main_EndError()


; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_Result
; Description ...: Funkcja tworzy plik result.json na dysku workera
; Syntax ........: _Main_Result()
; Parameters ....: None
; Return values .: None
; Author ........: Piotr Wiśniewski © makeitright.pl
; ===============================================================================================================================
Func _Main_Result()
	Local $oJSONResult	; tworzymy obiekt JSON
	Json_Put($oJSONResult, ".status", $Main_G_LoggerStatus)
	Json_Put($oJSONResult, ".result", $Main_G_LoggerResult)
	For $Main_i = 0 To UBound($Main_G_aLoggerErrors,$UBOUND_ROWS)-1
		Json_Put($oJSONResult, ".error." & $Main_G_aLoggerErrors[$Main_i][0], $Main_G_aLoggerErrors[$Main_i][1])
	Next
	If UBound($Main_G_aResultReturns,$UBOUND_ROWS) > 0 Then
		For $Main_i=0 to UBound($Main_G_aResultReturns, $UBOUND_ROWS)-1
				Json_Put($oJSONResult, ".returns." & StringReplace($Main_G_aResultReturns[$Main_i][0], "|", "."), $Main_G_aResultReturns[$Main_i][1])
		Next
	EndIf
	Local $Main_Plik = FileOpen($Main_G_FolderWorkspace & "result\result.json",258)
	FileWrite($Main_Plik, Json_Encode($oJSONResult, $JSON_UNESCAPED_UNICODE))
	FileClose($Main_Plik)
EndFunc		; ==> _Main_Result()


; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_Logger
; Description ...: Zapisywanie danych do loga technicznego, biznesowego i/lub bezplikowego
; Syntax ........: _Main_Logger($Main_Komunikat[, $Main_Log = ""])
; Parameters ....:	$Main_Komunikat		- treść zapisywana do pliku log
;                 	$Main_Log			- [OPCJA] sterowanie gdzie dodatkowo ma być zalogowany komunikat, kody sterujące można łączyć, np. "rb"
;						"b" = treść zapisywana do pliku loga biznesowego,
;						"n" = treść zapisywana tylko do pliku loga biznesowego,
;						"r" = treść jest zapisywana w pamięci komputera (ze względu na RODO)
; Return values .: treść $Main_Komunikat
; Author ........: Piotr Wiśniewski, Mateusz Kłosowski © makeitright.pl
; Modified ......: 2019-03-12 - usunięto rozszerzenie pliku "log" dla $Main_G_FileLog i $Main_G_FileLogBusiness
;                             - zmiana nazw zmiennych na ANG
; ===============================================================================================================================
Func _Main_Logger($Main_Komunikat, $Main_Log = "")
	$Main_Komunikat = StringReplace($Main_Komunikat, @CRLF, @CRLF & @TAB & @TAB & @TAB)	; change ENTER to ENTER & 3xTAB
	; sprawdzenie czy komunikat ma lecieć wyłącznie do loga bezplikowego
	If (Not ($Main_Log == "r")) And (StringInStr($Main_Log, "n") = 0) Then
		FileWriteLine($Main_G_FileLog,@CRLF & "[" & _NowTime() & "] " & $Main_Komunikat)
	EndIf
	; komunikat leci TEŻ do loga business
	If StringInStr($Main_Log, "b") Or StringInStr($Main_Log, "n") Then
		FileWriteLine($Main_G_FileLogBusiness,@CRLF & "[" & _NowTime() & "] " & $Main_Komunikat)
	EndIf
	; komunikat leci m.in. do loga bezplikowego
	If StringInStr($Main_Log, "r") And Not StringInStr($Main_Log, "n") Then
		If $Main_G_NoFileLogResultIndex >= UBound($Main_G_NoFileLogResultEntries) - 1 Then _
			ReDim $Main_G_NoFileLogResultEntries[UBound($Main_G_NoFileLogResultEntries) + 99]
		$Main_G_NoFileLogResultEntries[$Main_G_NoFileLogResultIndex] = "[" & _NowTime() & "] " & $Main_Komunikat
		$Main_G_NoFileLogResultIndex += 1
	EndIf
	Return $Main_Komunikat
EndFunc   ; ==> _Main_Logger()


; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_LoggerError
; Description ...: Formatowanie i zapisywanie danych błędu do loga technicznego, biznesowego i/lub bezplikowego
; Syntax ........: _Main_LoggerError($message, $Main_Stack_Trace[, $Main_CausedBy=Default][, $Main_Log="br"])
; Parameters ....:	$message	- treść zapisywana do pliku log
;					$Main_CausedBy	- [OPCJA] funkcja, która spowodowała błąd
;                 	$Main_Log		- [OPCJA] sterowanie gdzie dodatkowo ma być zalogowany komunikat, kody sterujące można łączyć, np. "rb"
;						"b" = treść zapisywana do pliku loga biznesowego,
;						"n" = treść zapisywana tylko do pliku loga biznesowego,
;						"r" = treść jest zapisywana w pamięci komputera (ze względu na RODO)
; Return values .: sformatowana treść komunikatu błędu (bez prefixu 'ERROR')
; Author ........: Mateusz Kłosowski © makeitright.pl
; ===============================================================================================================================
Func _Main_LoggerError($Main_Message, $Main_StackTrace, $Main_CausedBy=Default, $Main_Log="br")
	Local $Main_InfoTech, $Main_InfoBiz
	; przygotowanie treści komunikatów
		$Main_InfoTech = "Komunikat błędu: " & $Main_Message
		If Not IsKeyword($Main_CausedBy) And $Main_CausedBy <> "" Then _
			$Main_InfoTech = $Main_InfoTech & @CRLF & " Przyczyna błędu: " & $Main_CausedBy
		$Main_InfoBiz = $Main_InfoTech
		If $Main_StackTrace <> "" Then _
			$Main_InfoTech = "Podczas działania funkcji '" & $Main_StackTrace & "' wystąpił błąd. " & @CRLF & $Main_InfoTech
		If IsKeyword($Main_Log) Then $Main_Log = ""
	; wysyłka do Loggera
		If $Main_Log == "r" Then
			_Main_Logger("[BŁĄD] : " & $Main_InfoBiz, "r")
		Else
			If StringInStr($Main_Log, "r") Then _
				_Main_Logger("[BŁĄD] : " & $Main_InfoBiz, "r")
			If StringInStr($Main_Log, "b") Or StringInStr($Main_Log, "n") Then _
				_Main_Logger("[BŁĄD] : " & $Main_InfoBiz, "n")
			If StringInStr($Main_Log, "n") = 0 Then _
				_Main_Logger("[BŁĄD] : " & $Main_InfoTech, "")
		EndIf
	Return $Main_InfoTech
EndFunc   ; ==> _Main_LoggerError()





; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_LoadArgumentsOptions
; Description ...: Function reading data from file arguments.json or options.json to array $Main_G_aArguments or $Main_G_aOptions
;						UWAGA: parsowany jest tylko pierwszy poziom json'a (!!!)
; Syntax ........: _Main_LoadArgumentsOptions($Main_Parameter)
; Parameters ....: $Main_Parameter            - "arguments" or "options"
; Return values .: Array with data read from file arguments.json or options.json
; Author ........: Piotr Wiśniewski, Mateusz Kłosowski © makeitright.pl
; Modified ......: 2019-03-12 - zmiana nazw zmiennych na ANG
;                             - poprawiono obsługę błędnego $Main_Parameter
; ===============================================================================================================================
Func _Main_LoadArgumentsOptions($Main_Parameter)
	If $Main_Parameter <> "arguments" And $Main_Parameter <> "options" Then _
		_Main_EndError("Wywołano funkcję standard/_Main_LoadArgumentsOptions($Main_Parameter) z błędnym parametrem: " & $Main_Parameter, _
				"standard/_Main_LoadArgumentsOptions($parametr)", "Błędny parametr "  & $Main_Parameter & " w wywołaniu funkcji")
	If FileExists ($Main_G_FolderWorkspace & $Main_Parameter & ".json") Then
		Local $Main_File = FileOpen($Main_G_FolderWorkspace & $Main_Parameter & ".json",0)
		If @error = -1 Then _
			_Main_EndError("Funkcja _Main_LoadArgumentsOptions($parametr) Nie moze otworzyć pliku na dysku: " & $Main_G_FolderWorkspace & $Main_Parameter & ".json")
		Local $Main_OdczytanyString = FileRead($Main_File) ; odczytujemy plik do stringa
		FileClose($Main_File) ; zamykamy plik

		Local $Main_Json = Json_Decode($Main_OdczytanyString) ; decode json
		Local $Main_oKeys = Json_ObjGetKeys($Main_Json)		; pobieramy klucze główne
		Local $Main_oSubJson, $oSubKeys, $Main_sNameMainKey, $sValueMainKey, $Main_sNameSubKey, $Main_sValueSubKey
			For $Main_i = 0 to Ubound($Main_oKeys)-1
				$Main_sNameMainKey = $Main_oKeys[$Main_i]
				$sValueMainKey = Json_Get($Main_Json, "." & $Main_oKeys[$Main_i])
				$Main_oSubJson = Json_ObjGet($Main_Json, $Main_oKeys[$Main_i])
				If Json_IsObject($Main_oSubJson) Then	;klucz ma podklucze
					$oSubKeys = Json_ObjGetKeys($Main_oSubJson)		; pobieramy podklucze
					For $Main_ii = 0 to Ubound($oSubKeys)-1
						$Main_sNameSubKey = $oSubKeys[$Main_ii]
						$Main_sValueSubKey = Json_ObjGet($Main_oSubJson, $Main_sNameSubKey)
						;_CW($Main_sNameMainKey &"|"&$Main_sNameSubKey & "  =  " & $Main_sValueSubKey)
						If $Main_Parameter = "options" Then
							ReDim $Main_G_aOptions[Ubound($Main_G_aOptions, $UBOUND_ROWS)+1][2]
							$Main_G_aOptions[Ubound($Main_G_aOptions, $UBOUND_ROWS)-1][0] = $Main_sNameMainKey & "|" &$Main_sNameSubKey ;nazwa klucza opcji
							$Main_G_aOptions[Ubound($Main_G_aOptions, $UBOUND_ROWS)-1][1] = $Main_sValueSubKey	; wartość opcji
						ElseIf $Main_Parameter = "arguments" Then
							ReDim $Main_G_aArguments[Ubound($Main_G_aArguments, $UBOUND_ROWS)+1][2]
							$Main_G_aArguments[Ubound($Main_G_aArguments, $UBOUND_ROWS)-1][0] = $Main_sNameMainKey & "|" &$Main_sNameSubKey ;nazwa klucza opcji
							$Main_G_aArguments[Ubound($Main_G_aArguments, $UBOUND_ROWS)-1][1] = $Main_sValueSubKey	; wartość opcji
						EndIf
					Next
				Else
					If $Main_Parameter = "options" Then
						ReDim $Main_G_aOptions[Ubound($Main_G_aOptions, $UBOUND_ROWS)+1][2]
						$Main_G_aOptions[Ubound($Main_G_aOptions, $UBOUND_ROWS)-1][0] = $Main_sNameMainKey ;nazwa klucza opcji
						$Main_G_aOptions[Ubound($Main_G_aOptions, $UBOUND_ROWS)-1][1] = $sValueMainKey	; wartość opcji
					ElseIf $Main_Parameter = "arguments" Then
						ReDim $Main_G_aArguments[Ubound($Main_G_aArguments, $UBOUND_ROWS)+1][2]
						$Main_G_aArguments[Ubound($Main_G_aArguments, $UBOUND_ROWS)-1][0] = $Main_sNameMainKey ;nazwa klucza opcji
						$Main_G_aArguments[Ubound($Main_G_aArguments, $UBOUND_ROWS)-1][1] = $sValueMainKey	; wartość opcji
					EndIf
					;_CW($Main_sNameMainKey & "  =  " & $sValueMainKey)
				EndIf
			Next
	EndIf
EndFunc   ; ==> _Main_LoadArgumentsOptions()


; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_GetArgument
; Description ...: Funkcja zwraca wartość wywołanego argumentu z arguments.json
; Syntax ........: _Main_GetArgument($Argument)
; Parameters ....: $Argument            - name key argument
; Return values .: string or @error=1 and Null
; Author ........: Piotr Wiśniewski © makeitright.pl
; Modified ......: 2019-03-12 - zmiana nazw zmiennych na ANG
;                  2019-03-14 - modified return value
; ===============================================================================================================================
Func _Main_GetArgument($Argument)
	Local $Main_Counter = _ArraySearch($Main_G_aArguments,$Argument)
	If $Main_Counter > -1 Then
		Return $Main_G_aArguments[$Main_Counter][1]
	Else
		Return SetError(1,0,Null)
	EndIf
EndFunc   ; ==> _Main_GetArgument()

; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_GetOption
; Description ...: Funkcja zwraca wartość wywołanego parametru z parametr.json
; Syntax ........: _Main_GetOption($Options)
; Parameters ....: $Options            - name key option
; Return values .: string or @error=1 and Null
; Author ........: Piotr Wiśniewski © makeitright.pl
; Modified ......: 2019-03-12 - zmiana nazw zmiennych na ANG
;                  2019-03-14 - modified return value
; ===============================================================================================================================
Func _Main_GetOption($Options)
	Local $Main_Counter = _ArraySearch($Main_G_aOptions,$Options)
	If $Main_Counter > -1 Then
		Return $Main_G_aOptions[$Main_Counter][1]
	Else
		Return SetError(1,0,Null)
	EndIf
EndFunc   ; ==> _Main_GetOption()

; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_IsArgument
; Description ...: Function check if exists key argument in $Main_G_aArguments (loaded from arguments.json)
; Syntax ........: _Main_IsArgument($Argument)
; Parameters ....: $Argument            - name key argument
; Return values .: 1 if key argument exists
;                  0 if key argument NOT exists
; Author ........: Piotr Wiśniewski © makeitright.pl
; Modified ......:
; ===============================================================================================================================
Func _Main_IsArgument($Argument)
	Local $Main_Counter = _ArraySearch($Main_G_aArguments,$Argument)
	If $Main_Counter > -1 Then
		Return 1
	Else
		Return 0
	EndIf
EndFunc   ; ==> _Main_IsArgument()

; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_IsOption
; Description ...: Function check if exists key option in $Main_G_aOptions (loaded from options.json)
; Syntax ........: _Main_IsOption($Option)
; Parameters ....: $Option            - name key option
; Return values .: 1 if key option exists
;                  0 if key option NOT exists
; Author ........: Piotr Wiśniewski © makeitright.pl
; Modified ......:
; ===============================================================================================================================
Func _Main_IsOption($Option)
	Local $Main_Counter = _ArraySearch($Main_G_aOptions,$Option)
	If $Main_Counter > -1 Then
		Return 1
	Else
		Return 0
	EndIf
EndFunc   ; ==> _Main_IsOption()

; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_GetDecryptedPassword
; Description ...: Decrypt of encrypted string using provided key. The source string is hexadecimal.
;					This function is based on UFT algoritm created by Jacek Strach.
;					Here is optimised code and merged into a single function.
; Syntax ........: _Main_GetDecryptedPassword($Main_sCrypted, $Main_sKey)
; Parameters ....: $hexCrypted		- string as hexadecimal
;					$Main_sKey			- string key
; Return values .: Decrypted string.
; Author ........: Mateusz Kłosowski © Makeitright.pl
; ===============================================================================================================================
Func _Main_GetDecryptedPassword($Main_hexCrypted, $Main_sKey)
	Local $Main_sCrypted = "", $sbox[256], $Main_key[256], $Main_temp, $Main_a, $Main_b = 0, $Main_i = 0, $Main_j = 0, $Main_k, $Main_decipherByte, $Main_decipher = ""
	Local $Main_strLength = StringLen($Main_sKey)
	; change hexadecimal to actual string (Hex2Text)
	For $Main_h = 1 To StringLen($Main_hexCrypted) Step 2
		$Main_sCrypted &= Chr(Dec(StringMid($Main_hexCrypted, $Main_h, 2)))
	Next
	; prepare bytes arrays (RC4Initialize)
	For $Main_a = 0 To 255
		$Main_key[$Main_a] = Asc(StringMid($Main_sKey, Mod($Main_a, $Main_strLength) + 1, 1))
		$sbox[$Main_a] = $Main_a
	Next
	For $Main_a = 0 To 255
		$Main_b = Mod (($Main_b + $sbox[$Main_a] + $Main_key[$Main_a]), 256)
		$Main_temp = $sbox[$Main_a]
		$sbox[$Main_a] = $sbox[$Main_b]
		$sbox[$Main_b] = $Main_temp
	Next
	; decrypt
	For $Main_a = 1 To StringLen($Main_sCrypted)
		$Main_i = Mod(($Main_i + 1), 256)
		$Main_j = Mod(($Main_j + $sbox[$Main_i]), 256)
		$Main_temp = $sbox[$Main_i]
		$sbox[$Main_i] = $sbox[$Main_j]
		$sbox[$Main_j] = $Main_temp
		$Main_k = $sbox[Mod($sbox[$Main_i] + $sbox[$Main_j], 256)]
		$Main_decipherByte = BitXOR(Asc(StringMid($Main_sCrypted, $Main_a, 1)), $Main_k)
		$Main_decipher &= Chr($Main_decipherByte)
	Next
	Return $Main_decipher
EndFunc   ; ==> _Main_GetDecryptedPassword



; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_SetResultReturns
; Description ...: Modified (if exists) or addes (if NOT exists) value $Key in array $Main_G_aResultReturns
; Syntax ........: _Main_SetResultReturns($Key, $Value)
; Parameters ....: $Key                 - key.
;                  $Value               - value.
; Return values .: Set #extended:
;                                1 if updated value exists key
;                                2 if add new record
; Author ........: Piotr Wisniewski ©makeitright.pl
; Modified ......:
; Example .......: No
; ===============================================================================================================================
Func _Main_SetResultReturns($Key, $Value)
	Local $Main_Counter = _ArraySearch($Main_G_aResultReturns,$Key)
	If $Main_Counter > -1 Then
		$Main_G_aResultReturns[$Main_Counter][1] = $Value
		Return SetExtended(1)
	Else
		ReDim $Main_G_aResultReturns[Ubound($Main_G_aResultReturns, $UBOUND_ROWS)+1][2]
		$Main_G_aResultReturns[Ubound($Main_G_aResultReturns, $UBOUND_ROWS)-1][0] = $Key	; add key
		$Main_G_aResultReturns[Ubound($Main_G_aResultReturns, $UBOUND_ROWS)-1][1] = $Value ; add value
		Return SetExtended(2)
	EndIf
EndFunc   ; ==> _Main_SetResultReturns()

; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_ReadResultReturns
; Description ...: Funkcja zwraca wartość podanego klucza z tablicy przechowującej dla result
; Syntax ........: _Main_ReadResultReturns($Key)
; Parameters ....: $Key            - name key
; Return values .: string or @error=1 and Null
; Author ........: Piotr Wiśniewski © makeitright.pl
; ===============================================================================================================================
Func _Main_ReadResultReturns($Key)
	Local $Main_Counter = _ArraySearch($Main_G_aResultReturns,$Key)
	If $Main_Counter > -1 Then
		Return $Main_G_aResultReturns[$Main_Counter][1]
	Else
		Return SetError(1,0,Null)
	EndIf
EndFunc   ; ==> _Main_ReadResultReturns()

; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_ScreenShot
; Description ...: Wykonanie zrzutu ekranowego aplikacji lub całego pulpitu.
; Syntax ........: _Main_ScreenShot([$Main_appHWND = Default]), _Main_AppScreenShot("FULL")
; Parameters ....: $Main_appHWND		- [optional] uchwyt aplikacji, jeśli brak brany jest domyślny ze zmiennej globalnej $Main_G_Application
;                                                    "FULL" - wykonuje zrzut całego pulpitu
; Return values .:	ścieżka z nazwą pliku screenshot'a (w folderze result)
;					obiekt @error w przypadku błędu z pustym stringiem
; Author ........: Mateusz Kłosowski © makeitright.pl
; Modified ......: 2019-03-12 - $Main_G_FileLog zawiera PEŁNĄ nazwę pliku wraz z rozszerzeniem więc zmieniono: StringLen($Main_G_FileLog)-1) na StringLen($Main_G_FileLog)-4)
; ===============================================================================================================================
Func _Main_ScreenShot($Main_appHWND=Default)
	If IsKeyword($Main_appHWND) Then
		If IsKeyword($Main_G_Application) Then Return SetError(1, 0, "")
		$Main_appHWND = $Main_G_Application
	EndIf
	Local $Main_ssFileName = StringLeft($Main_G_FileLog, StringLen($Main_G_FileLog)-4) & "_" & StringRegExpReplace(_NowCalc(), ":|/|\h+", "") & ".png"
	If $Main_appHWND = "FULL" Then
		_ScreenCapture_Capture($Main_ssFileName)
	Else
		_ScreenCapture_CaptureWnd($Main_ssFileName, $Main_appHWND) ; wykonuje PrintScreen wskazanego okna aplikacji do katalogu POWERFARMER\workspace\result\attachments
	EndIf
	If @error Then
		_Main_Logger("Błąd podczas wykonywania zrzutu ekranowego.")
		Return SetError(2, 0, "")
	EndIf
	Return SetError(0, 0, $Main_ssFileName)
EndFunc		; ==> _Main_ScreenShot()


; #FUNCTION# ====================================================================================================================
; Name ..........: _Main_IsWorkstationLockedModern()
; Description ...: Check computer is locked
; Syntax ........: _Main_IsWorkstationLockedModern()
; Parameters ....: None
; Return values .: True if is Locked
;                  False if NOT Locked
; Author ........: https://www.autoitscript.com/forum/topic/191368-check-if-workstation-is-locked-cross-platform/
; Modified ......:
; Remarks .......:
; Related .......:
; Link ..........:
; Example .......: No
; ===============================================================================================================================
Func _Main_IsWorkstationLockedModern()
    Local Const $WTS_CURRENT_SERVER_HANDLE = 0
    Local Const $WTS_CURRENT_SESSION = -1
    Local Const $WTS_SESSION_INFO_EX = 25
    Local $hWtsapi32dll = DllOpen("Wtsapi32.dll")
    Local $result = DllCall($hWtsapi32dll, "int", "WTSQuerySessionInformation", "int", $WTS_CURRENT_SERVER_HANDLE, "int", $WTS_CURRENT_SESSION, "int", $WTS_SESSION_INFO_EX, "ptr*", 0, "dword*", 0)
    If ((@error) OR ($result[0] == 0)) Then
        Return SetError(1, 0, False)
    EndIf
    Local $buffer_ptr = $result[4]
    Local $buffer_size = $result[5]
    Local $buffer = DllStructCreate("uint64 SessionId;uint64 SessionState;int SessionFlags;byte[" & $buffer_size - 20 & "]", $buffer_ptr)
    Local $isLocked = (DllStructGetData($buffer, "SessionFlags") == 0)
    $buffer = 0
    DllCall($hWtsapi32dll, "int", "WTSFreeMemory", "ptr", $buffer_ptr)
    DllClose($hWtsapi32dll)
    Return $isLocked
EndFunc




;=========================================================
;=========================================================
; SEKCJA TESTOWEGO URUCHAMIANIA FUNKCJI
;=========================================================

If @ScriptName = "Main.au3" Then
	Local $Main_iMsgBoxAnswer = MsgBox(36,"UWAGA !","Uruchomiono skrypt z komponentu lub biblioteki !" & @CRLF & "[TAK] - uruchom skrypt" & @CRLF & "[NIE] - przerwij działanie i zakończ skrypt")
	If $Main_iMsgBoxAnswer = 7 Then Exit;No
; od tego miejsca wykonywanie skryptu

Local $eee = $Main_G_aLoggerReturns[10][12]

EndIf