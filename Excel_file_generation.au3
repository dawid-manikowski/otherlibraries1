﻿;----------------------------------------------------------------------------------------------
; Copyright Jerome DERN 2010
; ---------------------------------------------------------------------------------------------
#Include <File.au3>

; Const for cell format
Const $XMLXLS_STRING    = "String"
Const $XMLXLS_NUMBER    = "Number"
Const $XMLXLS_SDATE     = "Short Date"
Const $XMLXLS_PERCENT   = "Percent"
Const $XMLXLS_SCIENTIFIC= "Scientific"
Const $XMLXLS_DATE      = "[$-F400]h:mm:ss\ AM/PM"
Const $XMLXLS_FRACTION  = "#&quot; &quot;?/?"
Const $XMLXLS_COMPTAB   = "_-* #,##0.00\ &quot;â‚¬&quot;_-;\-* #,##0.00\ &quot;â‚¬&quot;_-;_-* &quot;-&quot;??\ &quot;â‚¬&quot;_-;_-@_-"
; Constants for cell alignment
Const $XMLXLS_CENTER    = "Center"
Const $XMLXLS_RIGHT     = "Right"
Const $XMLXLS_LEFT  = "Left"
Const $XMLXLS_BOTTOM    = "Bottom"
Const $XMLXLS_TOP   = "Top"
Const $XMLXLS_JUSTIFIED = "Justify"
; Font style
Global Enum Step *2 $XMLXLS_BOLD, $XMLXLS_ITALIC, $XMLXLS_STRIKETHROUGH, $XMLXLS_UNDERLINE, $XMLXLS_SUBSCRIPT, $XMLXLS_SUPERSCRIPT
; BitOr Values for border
Global Enum Step *2 $XMLXLS_BDRIGHT, $XMLXLS_BDLEFT, $XMLXLS_BDTOP, $XMLXLS_BDBOTTOM, $XMLXLS_BDCROSS1, $XMLXLS_BDCROSS2
; Page Set up
Const $XMLXLS_LANDSCAPE = "Landscape"
Const $XMLXLS_PORTRAIT = "Portrait"

;~ $demo1 = False

;~ $file = FileOpen("text.xls", 2)
;~ ; Create Workbook, active sheet will be se second one (2)
;~ _XLSCreateWorkbook($file, 2)
;~ ; Define styles
;~ _XLSCreateStyles($file)
;~ ; Define a new personal style
;~ _XLSAddStyle($file, "Header", "Arial", 11, $XMLXLS_BOLD, "000000", "", $XMLXLS_CENTER, $XMLXLS_CENTER, BitOR($XMLXLS_BDRIGHT, $XMLXLS_BDLEFT, $XMLXLS_BDTOP, $XMLXLS_BDBOTTOM))
;~ _XLSAddStyle($file, "Table", "Arial", 11, 0, "000000", "", $XMLXLS_LEFT, $XMLXLS_CENTER, BitOR($XMLXLS_BDRIGHT, $XMLXLS_BDLEFT, $XMLXLS_BDTOP, $XMLXLS_BDBOTTOM))
;~ ; Finish style definition
;~ _XLSCloseStyles($file)

; In demo1 case the sheets are generated from an array in memory
; limitation: named area can't be defined



;~ If $demo1 Then
;~     ; a typical excel sheet memory reservation (can be adjusted to real need)
;~     Dim $Sheet1[65536][255]

;~     ; Create sheet 1
;~     _XLSInitSheet($Sheet1, "Page 1")
;~     _XLSSetRowHeight($Sheet1, 1, 40)
;~     _XLSSetColumnWidth($Sheet1, 1, 100)
;~     _XLSSetColumnWidth($Sheet1, 2, 110)
;~     _XLSSetColumnWidth($Sheet1, 3, 120)
;~     _XLSSetCell($Sheet1, 1, 1, "Qty", "", "", "Header")
;~     _XLSSetCell($Sheet1, 1, 2, "Value", "", "", "Header")
;~     _XLSSetCell($Sheet1, 1, 3, "Total", "", "", "Header")
;~     _XLSSetCell($Sheet1, 2, 1, 2, "", $XMLXLS_NUMBER, "Table", "qty")
;~     _XLSSetCell($Sheet1, 2, 2, 4, "", $XMLXLS_NUMBER, "Table", "value")
;~     ;_XLSSetCell($Sheet1, 2, 3, 0, "RC[-2]*RC[-1]", $XMLXLS_NUMBER, "Table")
;~     _XLSSetCell($Sheet1, 2, 3, 0, "qty*value", $XMLXLS_NUMBER, "Table")
;~     _XLSGenerateFromArray($file, $Sheet1, $XMLXLS_LANDSCAPE)

;~ EndIf
;~ ; Close Worksbook content, file is ready to use
;~ _XLSCloseWorkBook($file)
;~ ShellExecute("text.xls")

; -------------------------------------------------------------------------------------------------------------------------------------
; ------------------------------------------------------- ARRAY TO XML PRIMITIVES -----------------------------------------------------
; -------------------------------------------------------------------------------------------------------------------------------------

; -----------------------------------------------------------------------------------
; Initialize sheet : Set sheet name and set active size to 1 cell
; -----------------------------------------------------------------------------------
Func _XLSInitSheet(ByRef $array, $name)
    ; $array[0][0] store the name of the sheet;the maximum row number used;the maximum column number used
    $array[0][0] = $name & ";1;1"
EndFunc

; -----------------------------------------------------------------------------------
; Set row height
; -----------------------------------------------------------------------------------
Func _XLSSetRowHeight(ByRef $array, $row, $height)
    ; array[x][0] store the height of each row
    $array[$row][0] = $height
EndFunc

Func _XLSSetColumnWidth(ByRef $array, $col, $width)
    ; array[0][x] store the width of each column
    $array[0][$col] = $width
EndFunc

; ------------------------------------------------------------------------------------
; This format a cell in case of an array usage + generation with _XLSGenerateFromArray function
; ------------------------------------------------------------------------------------
Func _XLSSetCell(ByRef $array, $row, $col, $data, $formulae, $type=$XMLXLS_STRING, $style="Default", $name="")
    Local $decode
    If $formulae<>"" Then $data=""
    if $type = "" Then $type = $XMLXLS_STRING
    If $style = "" Then $style = "Default"
    $array[$row][$col] = $data & @CR & $formulae & @CR & $type & @CR & $style & @CR & $name
    ; check if max of row and column are to be updated
    $decode = StringSplit($array[0][0], ";")
    If $decode[0]<3 Then Return
    If $row>$decode[2] Then $decode[2]=$row
    If $col>$decode[3] Then $decode[3]=$col
    $array[0][0]=$decode[1]&";"&$decode[2]&";"&$decode[3]
EndFunc

; ----------------------------------------------------------------------------------
; Generate a sheet from a 2D array. array[0][0] has the sheet name, other array[row][col]
; are strings formatted using _XLSBuildCell function
; ----------------------------------------------------------------------------------
Func _XLSGenerateFromArray($file, ByRef $array, $orientation=$XMLXLS_PORTRAIT, $mbottom = 0.98, $mleft=0.78, $mright=0.78, $mtop=0.98, $hmargin=0.5, $fmargin=0.5)
    Local $col, $row, $sname, $decode, $data, $formula, $type, $style, $name, $height
    ; get array real size
    $decode = StringSplit($array[0][0], ";")
    If $decode[0] < 3 Then Return
    $sname = $decode[1]
    $maxrow = $decode[2]
    $maxcol = $decode[3]
    ; Create a new worksheet
    _XLSAddWorkSheet($file, $sname)
    ; Define specific names of this sheet (optional)
    _XLSCreateWSNames($file)
    ; Print area is a specific name for printing zone
    _XLSAddWSName($file, "Print_Area", "'"&$sname&"'" & "!R1C1:R"&$maxrow&"C"&$maxcol)
    ; Small patch in order to Manage here named cells
    Local $temp = $file, $nbnames=0
    Dim $cellnames[1000][2]
    $file =_TempFile()
    ; Finish definition of names
    _XLSCloseWSNames($file)
    ; Start Worksheet content
    _XLSStartWSDefinition($file)
    ; Write column size if needed
    For $col=1 to $maxcol
        If $array[0][$col] <> "" Then _XLSColumnSize($file, $col, $array[0][$col])
    Next
    ; generate sheet content
    For $row=1 to $maxrow
        $height = -1
        If $array[$row][0] <> "" Then $height = $array[$row][0]
        ; Create a row
        _XLSAddRowInWorkSheet($file, $height)
        For $col=1 to $maxcol
            $data = ""
            $formula = ""
            $type = $XMLXLS_STRING
            $style = "Default"
            $name = ""
            If $array[$row][$col]<>"" Then
                $e = StringSplit($array[$row][$col], @CR)
                If $e[0]=5 Then
                    $data   = $e[1]
                    $formula = $e[2]
                    $type   = $e[3]
                    $style = $e[4]
                    $name   = $e[5] ;, named cell not yet implemented
                    If $name<>"" Then
                        $cellnames[$nbnames][0] = $name
                        $cellnames[$nbnames][1] = "'"&$sname&"'"&"!R"&$row&"C"&$col
                        $nbnames+=1
                    EndIf
                EndIf
            EndIf
            If $type="" Then $type = $XMLXLS_STRING
            If $style="" Then $style = "Default"
            ; Add data to first row, column 1
            _XLSAddCellInRow($file, $data, $formula, $type, $style)
        Next
        ; Finish row definition
        _XLSCloseRow($file)
    Next
    ; Add named cell found
    For $col=1 to $nbnames
        _XLSAddWSName($temp, $cellnames[$col-1][0], $cellnames[$col-1][1])
    Next
    ; merge files
    Dim $aRecords
    _FileReadToArray($file,$aRecords)
    FileDelete($file)
    $file = $temp
    For $col = 1 to $aRecords[0]
        FileWriteLine($file, $aRecords[$col])
    Next
    ; Close Worksheet content
    _XLSCloseWorkSheet($file, $orientation, $mbottom, $mleft, $mright, $mtop, $hmargin, $fmargin)
EndFunc

; -------------------------------------------------------------------------------------------------------------------------------------
; ------------------------------------------------------- XML BASE PRIMITIVES ---------------------------------------------------------
; -------------------------------------------------------------------------------------------------------------------------------------

; --------------------------------------------------------------------------------------------------
; Create (Define) a new Excel workbook
; --------------------------------------------------------------------------------------------------
Func _XLSCreateWorkbook($file, $activesheet=1)
     If IsString($file) Then FileDelete($file)
     $activesheet -= 1
     FileWriteLine($file, '<?xml version="1.0"?>')
	 FileWriteLine($file, '<?mso-application progid="Excel.Sheet"?>')
     FileWriteLine($file, '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">')
     FileWriteLine($file, '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>XLSXMLGenerator</Author><Created>'&@YEAR&'-'&@MON&'-'&@MDAY&'T'&@HOUR&':'&@MIN&':'&@SEC&'</Created><Company>XLSXML</Company></DocumentProperties>')
     FileWriteLine($file, '<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel"><ActiveSheet>'&$activesheet&'</ActiveSheet></ExcelWorkbook>')
EndFunc

; --------------------------------------------------------------------------------------------------
; Create (Define) a Style section
; --------------------------------------------------------------------------------------------------
Func _XLSCreateStyles($file)
     FileWriteLine($file, ' <Styles>')
     FileWriteLine($file, ' <Style ss:ID="Default" ss:Name="Normal"><Alignment ss:Vertical="Bottom"/><Borders/><Font ss:Size="11"/></Style>')
EndFunc

; --------------------------------------------------------------------------------------------------
; Create (Define) a new style. Borders must be constructed with BitOR() of desired border attribute
; --------------------------------------------------------------------------------------------------
Func _XLSAddStyle($file, $name="S21", $font="Arial", $size=11, $fstyle=0, $pcolor="000000", $icolor="", $AlignHor=$XMLXLS_CENTER, $AlignVer=$XMLXLS_BOTTOM, $Border=0)
     Local $f, $fontfam="Swiss"
     FileWriteLine($file, ' <Style ss:ID="' & $name &'">')
     FileWriteLine($file, '     <Alignment ss:Horizontal="' & $AlignHor & '" ss:Vertical="' & $AlignVer &'"/>')
     ;$Border = BitOR($XMLXLS_BDRIGHT, $XMLXLS_BDLEFT, $XMLXLS_BDTOP, $XMLXLS_BDBOTTOM, $XMLXLS_BDCROSS1, $XMLXLS_BDCROSS2)
     If $Border>0 Then FileWriteLine($file, '   <Borders>')
     If BitAnd($Border, $XMLXLS_BDBOTTOM)>0 Then FileWriteLine($file, '     <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>')
     If BitAnd($Border, $XMLXLS_BDLEFT)>0 Then FileWriteLine($file, '   <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>')
     If BitAnd($Border, $XMLXLS_BDRIGHT)>0 Then FileWriteLine($file, '  <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>')
     If BitAnd($Border, $XMLXLS_BDTOP)>0    Then FileWriteLine($file, '     <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>')
     If BitAnd($Border, $XMLXLS_BDCROSS1)>0 Then FileWriteLine($file, '     <Border ss:Position="DiagonalLeft" ss:LineStyle="Continuous" ss:Weight="1"/>')
     If BitAnd($Border, $XMLXLS_BDCROSS2)>0 Then FileWriteLine($file, '     <Border ss:Position="DiagonalRight" ss:LineStyle="Continuous" ss:Weight="1"/>')
     If $Border>0 Then FileWriteLine($file, '   </Borders>')
     $f = '     <Font ss:FontName="' & $font & '" x:Family="' & $fontfam & '" ss:Size="' & $size & '" '
     ;$fstyle = BitOR($XMLXLS_BOLD, $XMLXLS_ITALIC, $XMLXLS_STRIKETHROUGH, $XMLXLS_UNDERLINE, $XMLXLS_SUBSCRIPT)
     If BitAnd($fstyle, $XMLXLS_BOLD)>0     Then $f &= 'ss:Bold="1" '
     If BitAnd($fstyle, $XMLXLS_ITALIC)>0   Then $f &= 'ss:Italic="1" '
     If BitAnd($fstyle, $XMLXLS_STRIKETHROUGH)>0 Then $f &= 'ss:StrikeThrough="1" '
     If BitAnd($fstyle, $XMLXLS_UNDERLINE)>0    Then $f &= 'ss:Underline="Single" '
     If BitAnd($fstyle, $XMLXLS_SUBSCRIPT)>0    Then $f &= 'ss:VerticalAlign="Subscript" '
     If BitAnd($fstyle, $XMLXLS_SUPERSCRIPT)>0 Then $f &= 'ss:VerticalAlign="Superscript" '
     If $pcolor <>"" Then $f &= 'ss:Color="#'&$pcolor&'" '
     $f &= '/>'
     If $icolor<>"" Then FileWriteLine($file, '<Interior ss:Color="#'&$icolor&'" ss:Pattern="Solid"/>')
     FileWriteLine($file, $f)
     FileWriteLine($file, ' </Style>')
EndFunc

; --------------------------------------------------------------------------------------------------
; Close styles definitions
; --------------------------------------------------------------------------------------------------
Func _XLSCloseStyles($file)
     FileWriteLine($file, ' </Styles>')
EndFunc

; --------------------------------------------------------------------------------------------------
; Create (Define) a new Excel worksheet
; --------------------------------------------------------------------------------------------------
Func _XLSAddWorkSheet($file, $sheet)
     FileWriteLine($file, ' <Worksheet ss:Name="' & $sheet & '">')
EndFunc

; --------------------------------------------------------------------------------------------------
; Create (Define) an range named section
; --------------------------------------------------------------------------------------------------
Func _XLSCreateWSNames($file)
     FileWriteLine($file, ' <Names>')
EndFunc

; --------------------------------------------------------------------------------------------------
; Create (Define) a new range name
; --------------------------------------------------------------------------------------------------
Func _XLSAddWSName($file, $name="Print_Area", $location="'Sheet1'!R1C1:R65535C255")
    FileWriteLine($file, '  <NamedRange ss:Name="' & $Name & '" ss:RefersTo="=' & $location & '"/>')
     ; Ex: <NamedRange ss:Name="qty" ss:RefersTo="='Page 1'!R2C1"/>
    ; Ex: <NamedRange ss:Name="value" ss:RefersTo="='Page 1'!R2C2"/>
EndFunc

; --------------------------------------------------------------------------------------------------
; Close Worksheet named range section
; --------------------------------------------------------------------------------------------------
Func _XLSCloseWSNames($file)
     FileWriteLine($file, ' </Names>')
EndFunc

; --------------------------------------------------------------------------------------------------
; Begin WorkSheet definition
; --------------------------------------------------------------------------------------------------
Func _XLSStartWSDefinition($file)
     FileWriteLine($file, ' <Table x:FullColumns="1" x:FullRows="1">')
EndFunc

; --------------------------------------------------------------------------------------------------
; Define column size
; --------------------------------------------------------------------------------------------------
Func _XLSColumnSize($file, $col, $size)
     FileWriteLine($file, '     <Column ss:Index="'&$col&'" ss:AutoFitWidth="0" ss:Width="'&$size&'"/>')
EndFunc

; --------------------------------------------------------------------------------------------------
; Add a new row in current worksheet
; --------------------------------------------------------------------------------------------------
Func _XLSAddRowInWorkSheet($file, $height=-1)
     If $height = -1 Then
        FileWriteLine($file, '  <Row ss:AutoFitHeight="1">')
    Else
        FileWriteLine($file, '  <Row ss:AutoFitHeight="0" ss:Height="'& $height &'">')
    EndIf
EndFunc

; --------------------------------------------------------------------------------------------------
; Add a cell (column) in the cirrent row of the current worksheet
; --------------------------------------------------------------------------------------------------
Func _XLSAddCellInRow($file, $data, $formula="", $type=$XMLXLS_STRING, $style="Default")
     Local $Cell ='     <Cell '
     If $formula <> "" Then $Cell &= 'ss:Formula="=' & $formula & '" '
     ; formulae example: RC[-1]*2 or R1C1*2
     ; formulae example: Sheet2!R[1]C*2
     FileWriteLine($file, $Cell & 'ss:StyleID="' & $style & '"><Data ss:Type="' & $type & '">' & $data & '</Data></Cell>')
EndFunc

; --------------------------------------------------------------------------------------------------
; Close current row definition
; --------------------------------------------------------------------------------------------------
Func _XLSCloseRow($file)
     FileWriteLine($file, '     </Row>')
EndFunc

; --------------------------------------------------------------------------------------------------
; Close current worksheet definition
; --------------------------------------------------------------------------------------------------
Func _XLSCloseWorkSheet($file, $orientation=$XMLXLS_PORTRAIT, $mbottom = 0.98, $mleft=0.78, $mright=0.78, $mtop=0.98, $hmargin=0.5, $fmargin=0.5)
     FileWriteLine($file, ' </Table>')
     FileWriteLine($file, '     <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">')
    FileWriteLine($file, '  <PageSetup>')
    FileWriteLine($file, '  <Layout x:Orientation="'&$orientation&'"/>')
    FileWriteLine($file, '  <Header x:Margin="'&$hmargin&'"/>')
    FileWriteLine($file, '  <Footer x:Margin="'&$fmargin&'"/>')
    FileWriteLine($file, '  <PageMargins x:Bottom="'&$mbottom&'" x:Left="'&$mleft&'" x:Right="'&$mright&'" x:Top="'&$mtop&'"/>')
    FileWriteLine($file, '  </PageSetup>')
     FileWriteLine($file, '     </WorksheetOptions>')
     FileWriteLine($file, '     <ss:ActiveSheet>Page 2</ss:ActiveSheet>')
     FileWriteLine($file, ' </Worksheet>')
EndFunc

; --------------------------------------------------------------------------------------------------
; Close Excel workbook, the file now is readable by excel
; --------------------------------------------------------------------------------------------------
Func _XLSCloseWorkBook($file)
     FileWriteLine($file, '</Workbook>')
     If Not IsString($file) Then FileClose($file)
EndFunc

; -------------------------------------------------------------------------------
; Create a relative (to $l,$c) formulae to access to $l2,$c2 in sheet $sheet
; -------------------------------------------------------------------------------
Func _XLSlc2rc($l1, $c1, $l2, $C2, $sheet)
    Local $dc, $dl, $f
    $dc = $c2-$c1
    $dl = $l2-$l1
    $f="R"
    if $sheet <> "" Then $f = $sheet & "!" & $f
    if $dl<>0 Then $f &= '[' & $dl & ']'
    $f &= "C"
    if $dc<>0 Then $f &= '[' & $dc & ']'
    Return $f
EndFunc